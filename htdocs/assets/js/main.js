$('document').ready( function(){

	var mycycle;

	$('.comsub > a').click( function(e){
		e.preventDefault();
		$(this).parent().toggleClass('submenuaberto')
	});

	$('#toggleMenuMobile a').click( function(e){
		e.preventDefault();
		$(this).toggleClass('subAberto');
		$(this).parent().parent().find('#menuMobile').toggleClass('aberto');
	});

	var url_imagens = (window.location.href.indexOf("previa") > -1) ? '/previa/assets/images/layout/radio.png' : '/assets/images/layout/radio.png';

	$('input:radio').screwDefaultButtons({
		image: "url("+url_imagens+")",
		width:	 18,
		height:	 18
	});

	mycycle = $('#banners .animate').cycle({
		pager : $('#banners #controls')
	});

	$('input:radio').change( function(){
		$('.styledRadio').parent().toggleClass('marcado');
	});

	$('#enquete form').submit( function(e){
		e.preventDefault();
		var opcao = $(this).find('.radioMarcado input').val();

		if(opcao){
			var conteudoHtml = "<div class='pedirEmail alg-l'>";
				conteudoHtml += "<h1>Insira seu e-mail abaixo para confirmar seu voto:</h1>";
				conteudoHtml += "<form action='computarVoto' method='post'>";
					conteudoHtml += "<input type='email' name='emailVoto' placeholder='E-mail' required>";
					conteudoHtml += "<input type='hidden' name='opcaoVoto' value='"+opcao+"'>";
					conteudoHtml += "<input type='submit' value='confirmar voto &raquo;'>";
				conteudoHtml += "</form>";
			conteudoHtml += "</div>";
			$.colorbox({
				close : 'fechar',
				closeButton	: true,
				html : conteudoHtml,
				scrolling : false,
				width:600,
				height:205
			});
		}else{
			$.colorbox({
				close : 'fechar',
				closeButton	: true,
				html : "<div class='pedirEmail alg-c'><h1>Selecione uma resposta para a enquete.</h1></div>",
				scrolling : false,
				width:770,
				height:180
			});
		}


	});

	if($('#listaEquipe').length){
		var $container = $('#listaEquipe').masonry();
		$container.imagesLoaded( function() {
		  $container.masonry();
		});
	}

	window.addEventListener('resize', function(event){
		mycycle.cycle('destroy');
  		mycycle = $('#banners .animate').cycle({
			pager : $('#banners #controls')
		});
	});

	$('#galeria a').colorbox({
		scalePhotos : true,
		maxWidth : '95%',
		maxHeight : '95%',
	});

	if ($('.main-perguntasfrequentes').length) {
		$('.pergunta a').on('click', function(event) {
			event.preventDefault();

			$(this).parent().toggleClass('open')
			$(this).next().slideToggle();

			$('.pergunta').not($(this).parent()).removeClass('open').find('.resposta').slideUp();
		});
	}

	if($('.link-enviar').length){

		var titulo = $('#detalhes h1').html();
		var id_texto = $('#idtexto').html();

		var formCompartilhar = "";
		formCompartilhar += "<div class='shareForm'>";
			formCompartilhar += "<form action='' novalidate>";
				formCompartilhar += "<h2>Preencha os campos abaixo para enviar esta notícia:</h2>";
				formCompartilhar += "<h1>"+titulo+"</h1>";
				formCompartilhar += "<input type='text' name='from_nome' placeholder='Seu nome' required>";
				formCompartilhar += "<input type='email' name='from_email' placeholder='Seu e-mail' required>";
				formCompartilhar += "<input type='text' name='to_emails' placeholder='E-mails dos seus amigos (separados por vírgulas)' required>";
				formCompartilhar += "<input type='email' name='mensagem' placeholder='Mensagem (opcional)'>";
				formCompartilhar += "<div class='submit'>";
					formCompartilhar += "<input type='submit' value='enviar &raquo;'>";
				formCompartilhar += "</div>";
			formCompartilhar += "</form>";
			formCompartilhar += "<div class='resposta' style='text-align:center; display:none;'><h1>E-mail enviado com sucesso!</h1></div>";
		formCompartilhar += "</div>";

		$('.link-enviar').colorbox({
			title : false,
			width : '570',
			minHeight : '345',
			html : formCompartilhar,
			onComplete : function(){
				$('.shareForm form').submit( function(e){
					e.preventDefault();

					var nome = $(".shareForm form input[name=from_nome]").val();
					var email = $(".shareForm form input[name=from_email]").val();
					var destino = $(".shareForm form input[name=to_emails]").val();
					var mensagem = $(".shareForm form input[name=mensagem]").val();

					if(nome == ''){
						alert('Informe seu nome');
						return false;
					}
					if(email == ''){
						alert('Informe seu e-mail');
						return false;
					}
					if(destino == ''){
						alert('Informe o e-mail dos destinatários');
						return false;
					}
					$.post('compartilhar', {
						nome : nome,
						email : email,
						destino : destino,
						mensagem : mensagem,
						url : window.location.href,
						id : id_texto
					}, function(retorno){
						$('.shareForm form').fadeOut('fast', function(){
							$('.shareForm .resposta').fadeIn('fast', function(){
								setTimeout( function(){
									$.colorbox.close();
								}, 2000);
							});
						});
					});
				});
			}
		});
	}

});
