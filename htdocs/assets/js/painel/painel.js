jQuery(function($){
    $.datepicker.regional['pt-BR'] = {
        closeText: 'Fechar',
        prevText: '&#x3c;Anterior',
        nextText: 'Pr&oacute;ximo&#x3e;',
        currentText: 'Hoje',
        monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
        'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
        'Jul','Ago','Set','Out','Nov','Dez'],
        dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','S&aacute;bado'],
        dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
        dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 0,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''};
    $.datepicker.setDefaults($.datepicker.regional['pt-BR']);
});

$('document').ready( function(){

    $('.btn-delete').click( function(e){
        e.preventDefault();
        var form = $(this).closest('form');
        bootbox.confirm('Deseja Excluir o Registro?', function(result){
            if(result)
                form.submit();
            else
                $(this).modal('hide');
        });
    });

    $('table.table-sortable tbody').sortable({
        update : function () {
            serial = [];
            tabela = $('table.table-sortable').attr('data-tabela');
            $('table.table-sortable tbody').children('tr').each(function(idx, elm) {
                serial.push(elm.id.split('_')[1])
            });
            $.post('ajax/gravaOrdem', { data : serial , tabela : tabela });
        },
        helper: function(e, ui) {
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        },
        handle : $('.btn-move')
    }).disableSelection();

    $('.btn-move').click( function(e){e.preventDefault();});

    $('.datepicker').datepicker();

    if($('#opcoes-control').length){
        var original = "<div class='row'><div class='col-lg-12'><div class='input-group'><input type='text' class='form-control opcao-enquete' name='enquete_opcao[]' placeholder='Opção de Resposta'><span class='input-group-btn'><button class='btn btn-default btn-danger' type='button'><span class='glyphicon glyphicon-remove-circle'></span></button></span></div></div></div>";
        var placeholder = $('#listaOpcoes');

        $('#opcoes-control button').click( function(){
            placeholder.append(original);
        });

        $('body #listaOpcoes').on('click', '.row button.btn-danger', function(){
            var elPai = $(this).parent().parent().parent().parent();
            elPai.addClass('removendo');
            setTimeout( function(){
                elPai.remove();
            }, 450);
        });
    }

    if($('#selecionaDestaque').length){

        $("input[name='selecionaTipoDestaque']").change( function(){
            if($(this).val() == 'imagem'){
                $('#inputImagemdeDestaque').parent().removeClass('removido')
                $('#inputVídeodeDestaque').parent().addClass('removido');
            }else{
                $('#inputImagemdeDestaque').parent().addClass('removido')
                $('#inputVídeodeDestaque').parent().removeClass('removido');
            }
        });

        var marcado = $("input[name='selecionaTipoDestaque']:checked");
        if(marcado.length){
            if(marcado.val() == 'imagem'){
                $('#inputImagemdeDestaque').parent().removeClass('removido')
                $('#inputVídeodeDestaque').parent().addClass('removido');
            }else{
                $('#inputImagemdeDestaque').parent().addClass('removido')
                $('#inputVídeodeDestaque').parent().removeClass('removido');
            }
        }

    }

    $('#inputSelCategoria').change( function(){
        var slug_categoria = $(this).val();
        $.post('painel/pegarTextos', { slug_categoria : slug_categoria}, function(retorno){
            var retorno = JSON.parse(retorno);
            $('#inputSelTextos').find('option').remove().end();
            $.each(retorno , function(k, item) {
               $('#inputSelTextos')
                    .append($("<option></option>")
                        .attr("value", item.id)
                        .text(item.titulo)
                    );
            });
        });
    });

    $('.ativarenquente').click( function(){
        var id = $(this).attr('data-idenquete');
        bootbox.confirm("<br><h4>Deseja Ativar a Enquete?</h4><div class='alert alert-warning text-center' role='alert'>Ao ativar esta enquete, a enquete ativa anteriormente será desativa.<br>Caso ela já possua votos, será encerrada e arquivada no Histórico.</div>", function(result){
            if(result)
                window.location = 'painel/ativarEnquete/'+id;
            else
                $(this).modal('hide');
        });
    });

    if($('textarea').length){
        $('textarea.olho').ckeditor({
            removeButtons : 'Underline,Subscript,Superscript,InjectImage,oembed,Table'
        });
        $('textarea.perguntasfrequentes').ckeditor({
            removeButtons : 'Underline,Subscript,Superscript,InjectImage,oembed,Table,Link,Unlink,NumberedList,BulletedList'
        });
        $('textarea.cke').ckeditor();
        // CKEDITOR.stylesSet.add( 'custom_styles', [
            // Block-level styles
            // { name: 'Paragrafo Branco', element: 'p', styles: { 'margin' : '0', 'color' : '#fff', 'font-weight' : 'normal', 'font-family' : ''Varela Round', sans-serif', 'font-size' : '15px', 'line-height' : '130%' } },
        // ]);
    }

});
