/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For the complete reference:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config
	config.language = 'pt-br';

	config.extraPlugins = 'injectimage,oembed';

	config.allowedContent = true;
	config.forcePasteAsPlainText = true;

	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbarGroups = [];

	config.toolbar = [
		[ 'Bold', 'Italic', 'Underline', 'Subscript', 'Superscript' ],
		[ 'NumberedList', 'BulletedList' ],
	    [ 'Link', 'Unlink' ],
	    [ 'InjectImage', 'oembed', 'Table' ]
	];

	// Remove some buttons, provided by the standard plugins, which we don't
	// need to have in the Standard(s) toolbar.
	config.removeButtons = 'Underline,Subscript,Superscript';

	// Se the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

	// Make dialogs simpler.
	config.removeDialogTabs = 'image:advanced;link:advanced';

};
