<?php

Route::get('/', array('as' => 'home', 'uses' => 'HomeController@index'));
Route::get('home', array('as' => 'home', 'uses' => 'HomeController@index'));
Route::post('cadastroNewsletter', array('as' => 'cadastroNewsletter', 'uses' => 'HomeController@cadastro'));
Route::get('painel', array('before' => 'auth', 'as' => 'painel.home', 'uses' => 'Painel\HomeController@index'));
Route::get('painel/login', array('as' => 'painel.login', 'uses' => 'Painel\HomeController@login'));

Route::get('imprensa', array('as' => 'imprensa', 'uses' => 'ImprensaController@index'));
Route::get('perguntas-frequentes', array('as' => 'perguntasfrequentes', 'uses' => 'PerguntasFrequentesController@index'));

Route::get('quem-somos', array('as' => 'quemsomos', 'uses' => 'QuemSomosController@index'));
Route::get('equipe', array('as' => 'equipe', 'uses' => 'EquipeController@index'));
Route::get('contato', array('as' => 'contato', 'uses' => 'ContatoController@index'));
Route::post('enviarContato',  array('as' => 'enviarContato', 'uses' => 'ContatoController@enviar'));

Route::get('enquetes', array('as' => 'enquetes', 'uses' => 'EnquetesController@index'));
Route::get('enquetes/resultados/{dia?}/{mes?}/{ano?}/{id?}', array('as' => 'resultados', 'uses' => 'EnquetesController@resultados'));
Route::post('computarVoto', array('as' => 'votar', 'uses' => 'EnquetesController@votar'));

Route::get('noticias', array('as' => 'textos.noticias', 'uses' => 'TextosController@noticias'));
Route::get('noticias/{slug?}', array('as' => 'textos.noticias.detalhes', 'uses' => 'TextosController@detalhes'));
Route::get('reportagens', array('as' => 'textos.reportagens', 'uses' => 'TextosController@reportagens'));
Route::get('reportagens/{slug?}', array('as' => 'textos.reportagens.detalhes', 'uses' => 'TextosController@detalhes'));
Route::get('entrevistas', array('as' => 'textos.entrevistas', 'uses' => 'TextosController@entrevistas'));
Route::get('entrevistas/{slug?}', array('as' => 'textos.entrevistas.detalhes', 'uses' => 'TextosController@detalhes'));
Route::get('eventos', array('as' => 'textos.eventos', 'uses' => 'TextosController@eventos'));
Route::get('eventos/{slug?}', array('as' => 'textos.eventos.detalhes', 'uses' => 'TextosController@detalhes'));
Route::get('artigos', array('as' => 'textos.artigos', 'uses' => 'TextosController@artigos'));
Route::get('artigos/{slug?}', array('as' => 'textos.artigos.detalhes', 'uses' => 'TextosController@detalhes'));
Route::get('dicas', array('as' => 'textos.dicas', 'uses' => 'TextosController@dicas'));
Route::get('dicas/{slug?}', array('as' => 'textos.dicas.detalhes', 'uses' => 'TextosController@detalhes'));
Route::get('pesquisas', array('as' => 'textos.pesquisas', 'uses' => 'TextosController@pesquisas'));
Route::get('pesquisas/{slug?}', array('as' => 'textos.pesquisas.detalhes', 'uses' => 'TextosController@detalhes'));
Route::any('busca/{termo?}', array('as' => 'busca', 'uses' => 'TextosController@busca'));
Route::post('compartilhar', array('uses' => 'TextosController@compartilhar'));

Route::get('politica-de-privacidade', function(){
	\View::share('contatoLayout', Contato::first());
	\View::share('bannerInferior', BannerInferior::first());
	\View::share('politica', PoliticaPrivacidade::first());
	\View::share('bannerInferior', BannerInferior::ordenado());

	return \View::make('frontend.politica-privacidade.index');
});

Route::any('mais-recentes', function(){
	$textos = \Textos::orderBy('data_publicacao', 'DESC')->limit(5)->get();
	foreach ($textos as $key => $value) {
		$value->link = 'http://www.metanews.com.br/'.$value->categoria->slug.'/'.$value->slug;
		$value->capa_grande = 'http://www.metanews.com.br/assets/images/textos/capas/grande/'.$value->capa;
		$value->capa_pequena = 'http://www.metanews.com.br/assets/images/textos/capas/pequena/'.$value->capa;
		$value->titulo_categoria = $value->categoria->titulo;
	}
	return Response::json($textos);
});

// Autenticação do Login
Route::post('painel/login',  array('as' => 'painel.auth', function(){
	$authvars = array(
		'username' => Input::get('username'),
		'password' => Input::get('password')
	);

	$lembrar = (Input::get('lembrar') == '1') ? true : false;

	if(Auth::attempt($authvars, $lembrar)){
		return Redirect::to('painel');
	}else{
		Session::flash('login_errors', true);
		return Redirect::to('painel/login');
	}
}));

Route::get('painel/logout', array('as' => 'painel.off', function(){
	Auth::logout();
	return Redirect::to('painel');
}));

Route::post('ajax/gravaOrdem', array('before' => 'auth', 'uses' => 'Painel\AjaxController@gravaOrdem'));


Route::group(array('prefix' => 'painel', 'before' => 'auth'), function()
{
	Route::get('downloadCadastros', array('as' => 'painel.downloadCadastros', function(){

    	$filename = 'cadastros_'.date('d-m-Y-H-i-s');

		Excel::create($filename, function($excel) {
		    $excel->sheet('Usuários Cadastrados', function($sheet) {
		        $sheet->fromModel(\Cadastro::select('nome', 'email')->orderBy('nome')->get());
		    });
		})->download('csv');
    }));

	Route::post('pegarTextos', function(){
		$categoria = TextosCategorias::where('slug', '=', \Input::get('slug_categoria'))->first();
		return $categoria->textos->toJson();
	});

	Route::get('ativarEnquete/{id_enquete}', function($id_enquete){
		$todas = \EnquetePergunta::all();
		foreach ($todas as $key => $value) {
			$value->ativa = '0';
			$value->save();
		}
		$enquete = \EnquetePergunta::find($id_enquete);
		$enquete->ativa = '1';
		$enquete->save();
		return Redirect::back();
	});

    Route::resource('usuarios', 'Painel\UsuariosController');
    Route::resource('banners', 'Painel\BannersController');
	Route::resource('cadastros', 'Painel\CadastrosController');
	Route::resource('quemsomos', 'Painel\QuemSomosController');
	Route::resource('equipe', 'Painel\EquipeController');
	Route::resource('contato', 'Painel\ContatoController');
	Route::resource('enqueteperguntas', 'Painel\EnquetePerguntasController');
	Route::resource('textos', 'Painel\TextosController');
	Route::resource('destaques', 'Painel\DestaquesController');
	Route::resource('bannerinferior', 'Painel\BannerInferiorController');
	Route::resource('textosimagens', 'Painel\TextosImagensController');
	Route::resource('compartilhamentos', 'Painel\CompartilhamentosController');
	Route::resource('imprensa', 'Painel\ImprensaController');
	Route::resource('politica', 'Painel\PoliticaController');
	Route::resource('perguntasfrequentes/categorias', 'Painel\PerguntasFrequentesCategoriasController');
	Route::resource('perguntasfrequentes', 'Painel\PerguntasFrequentesController');
//NOVASROTASDOPAINEL//
});
