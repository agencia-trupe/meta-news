<?php

class PerguntaFrequente extends Eloquent
{
	protected $table = 'perguntas_frequentes';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
    	return $query->orderBy('ordem', 'asc')->orderBy('id', 'desc');
    }

    public function categoria()
    {
        return $this->belongsTo('PerguntaFrequenteCategoria', 'categoria_id');
    }
}
