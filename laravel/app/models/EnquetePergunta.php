<?php

class EnquetePergunta extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'enquete_perguntas';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

    public function scopeAtiva($query)
    {
        return $query->where('ativa', '=', '1')->first();
    }

    public function scopeSemvotos($query)
    {
    	$ids = \DB::table('enquete_votos')->groupBy('enquete_perguntas_id')->lists('enquete_perguntas_id');
    	
    	if(!$ids) 
    		$ids = array('');

    	return $query->whereNotIn('id', $ids)->orWhere('ativa', '=', '1');
    }

    public function scopeHistorico($query)
    {
    	$ids = \DB::table('enquete_votos')->groupBy('enquete_perguntas_id')->lists('enquete_perguntas_id');
    	
    	if(!$ids) 
    		$ids = array('');
    	
    	return $query->where('ativa', '!=', '1')->whereIn('id', $ids);
    }

    public function opcoes()
    {
    	return $this->hasMany('EnqueteOpcoes', 'enquete_pergunta_id')->orderBy('ordem', 'asc');
    }

    public function numeroVotos()
    {
    	return $this->hasMany('EnqueteVotos', 'enquete_perguntas_id');
    }
}