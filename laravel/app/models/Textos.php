<?php

class Textos extends Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'textos';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array();

    // array of attributes we CAN set on the backend
    protected $fillable = array('', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

    public function scopeDestaques($query)
    {
        return $query->where('destaque', '=', '1')->orderBy('textos_categorias_id', 'ASC')->orderBy('data_publicacao', 'desc')->get();
    }

    public function scopeNaodestaques($query)
    {
        return $query->where('destaque', '!=', '1')->orderBy('data_publicacao', 'desc')->get();
    }

    public function scopeCategoriaPorSlug($query, $slug_categoria)
    {
        return $query->join('textos_categorias', 'textos.textos_categorias_id', '=', 'textos_categorias.id')
                     ->where('textos_categorias.slug', '=', $slug_categoria)
                     ->orderBy('textos.data_publicacao', 'DESC');
    }

    public function scopeNoticias($query)
    {
        return $query->where('textos_categorias_id', '=', '1')->orderBy('data_publicacao', 'DESC');
    }

    public function scopeReportagens($query)
    {
        return $query->where('textos_categorias_id', '=', '6')->orderBy('data_publicacao', 'DESC');
    }

    public function scopeEntrevistas($query)
    {
        return $query->where('textos_categorias_id', '=', '7')->orderBy('data_publicacao', 'DESC');
    }

    public function scopeEventos($query)
    {
        return $query->where('textos_categorias_id', '=', '2')->orderBy('data_publicacao', 'DESC');
    }

    public function scopeArtigos($query)
    {
        return $query->where('textos_categorias_id', '=', '3')->orderBy('data_publicacao', 'DESC');
    }

    public function scopeDicas($query)
    {
        return $query->where('textos_categorias_id', '=', '4')->orderBy('data_publicacao', 'DESC');
    }

    public function scopePesquisas($query)
    {
        return $query->where('textos_categorias_id', '=', '5')->orderBy('data_publicacao', 'DESC');
    }

    public function scopeBuscaNoticias($query, $or)
    {
        return $or ? $query->orWhere('textos_categorias_id', '=', '1') : $query->where('textos_categorias_id', '=', '1');
    }
    public function scopeBuscaEventos($query, $or)
    {
        return $or ? $query->orWhere('textos_categorias_id', '=', '2') : $query->where('textos_categorias_id', '=', '2');
    }
    public function scopeBuscaArtigos($query, $or)
    {
        return $or ? $query->orWhere('textos_categorias_id', '=', '3') : $query->where('textos_categorias_id', '=', '3');
    }
    public function scopeBuscaDicas($query, $or)
    {
        return $or ? $query->orWhere('textos_categorias_id', '=', '4') : $query->where('textos_categorias_id', '=', '4');
    }
    public function scopeBuscaPesquisas($query, $or)
    {
        return $or ? $query->orWhere('textos_categorias_id', '=', '5') : $query->where('textos_categorias_id', '=', '5');
    }

    public function scopeRelacionados($query, $texto)
    {
        return $query->where('id', '!=', $texto->id)
                     ->where('textos_categorias_id', '=', $texto->textos_categorias_id)
                     ->orderBy('data_publicacao', 'DESC')
                     ->limit(5)
                     ->get();
    }

    public function scopeSearch($query,$q) {
      return empty($q) ? $query : $query->whereRaw("MATCH(titulo,olho,texto) AGAINST(? IN BOOLEAN MODE)",[$q]);
    }

    public function categoria()
    {
        return $this->belongsTo('TextosCategorias', 'textos_categorias_id');
    }

    public function imagens()
    {
        return $this->hasMany('TextosImagem', 'textos_id')->orderBy('ordem', 'ASC');
    }
}
