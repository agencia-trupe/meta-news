<?php

class PerguntaFrequenteCategoria extends Eloquent
{
    protected $table = 'perguntas_frequentes_categorias';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'asc')->orderBy('id', 'desc');
    }

    public function perguntas()
    {
        return $this->hasMany('PerguntaFrequente', 'categoria_id')->orderBy('ordem', 'desc')->orderBy('id', 'desc');
    }
}
