@extends('frontend.templates.index')

@section('conteudo')

	<div class="pure-g" id="detalhes-politica">

		<section class="pure-u-1 pure-u-mobile-1">
			<div class="pad">

				<h1>Política de Privacidade</h1>

				<div class="texto cke">
					{{\Tools::corrigePathImagens($politica->texto)}}
				</div>

			</div>
		</section>

	</div>

@stop