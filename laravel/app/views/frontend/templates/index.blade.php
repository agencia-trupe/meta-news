<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="robots" content="index, follow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2013 Trupe Design" />
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <meta name="keywords" content="" />
    <meta name="google-site-verification" content="kB2pt0rxzBJKQlA99qAovpFVWgdSgBbvvH2hSs0zyYo" />

	<title>Meta News @if(isset($htmlTitle)) {{$htmlTitle}} @endif</title>
	<meta name="description" content="">
	<meta property="og:title" content="Meta News @if(isset($htmlTitle)) {{$htmlTitle}} @endif"/>
	<meta property="og:description" content=""/>

    <meta property="og:site_name" content="Meta News"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="http://www.metanews.com.br/assets/images/layout/facebook-share.jpg"/>
    <meta property="og:url" content="{{ Request::url() }}?nocache=true"/>

	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>

	<?=Assets::CSS(array(
		'vendor/reset-css/reset',
		'css/fontface/stylesheet',
		'vendor/pure/grids',
		'vendor/pure/pure-start/grid',
		'vendor/fancybox/source/jquery.fancybox',
		'css/colorbox',
		'css/base',
		'css/'.str_replace('-', '', Route::currentRouteName())
	))?>

	@if(isset($css))
		<?=Assets::CSS(array($css))?>
	@endif

	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="{{asset('vendor/jquery/dist/jquery.min.js')}}"><\/script>')</script>

	@if(App::environment()=='local')
		<?=Assets::JS(array('js/modernizr', 'vendor/less.js/dist/less-1.6.2.min'))?>
	@else
		<?=Assets::JS(array('js/modernizr'))?>
	@endif

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57356005-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&appId=807926722562486&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
	<header>

		<div class="faixaAzul">
			<div class="centro">
				<div class="telefone">
					<span>{{$contatoLayout->prefixo_telefone}}</span> {{$contatoLayout->telefone}}
				</div>
				<div id="menuSuperior" class="hide-mobile">
					<nav>
						<ul>
							<li><a href="home" title="Página Inicial" @if(str_is('home', Route::currentRouteName())) class="ativo" @endif>HOME</a></li>
							<li><a href="quem-somos" title="Quem Somos" @if(str_is('quemsomos', Route::currentRouteName())) class="ativo" @endif>QUEM SOMOS</a></li>
							<li><a href="equipe" title="Equipe Meta News" @if(str_is('equipe', Route::currentRouteName())) class="ativo" @endif>EQUIPE</a></li>
							<li><a href="imprensa" title="Imprensa" @if(str_is('imprensa', Route::currentRouteName())) class="ativo" @endif>IMPRENSA</a></li>
							<li><a href="perguntas-frequentes" title="Perguntas Frequentes" @if(str_is('perguntasfrequentes', Route::currentRouteName())) class="ativo" @endif>PERGUNTAS FREQUENTES</a></li>
							<li><a href="contato" title="Contato" @if(str_is('contato', Route::currentRouteName())) class="ativo" @endif>CONTATO</a></li>
						</ul>
					</nav>
				</div>
				<div id="buscaSuperior">
					<form action="busca" method="post">
						<input type="text" name="termoBusca" placeholder="buscar...">
						<input type="submit" value="Buscar">
					</form>
				</div>
			</div>
		</div>

		<div class="faixaCinza">
			<div class="centro">
				<a href="home" title="Página Inicial" id="link-logo">
					<img src="assets/images/layout/logo.png" alt="Meta News">
				</a>

				<nav class="hide-mobile">
					<ul class="main-menu">
						<li class="comsub @if(str_is('textos.noticias*', Route::currentRouteName()) || str_is('textos.reportagens*', Route::currentRouteName()) || str_is('textos.entrevistas*', Route::currentRouteName())) ativo @endif">
							<a href="#" title="Notícias" class="linkMnNoticias @if(str_is('textos.noticias*', Route::currentRouteName()) || str_is('textos.reportagens*', Route::currentRouteName()) || str_is('textos.entrevistas*', Route::currentRouteName())) ativo @endif">NOTÍCIAS <span class="caret"></span></a>
							<ul class="sub">
								<li><a href="noticias" title="Notícias" @if(str_is('textos.noticias*', Route::currentRouteName())) class="ativo" @endif><span>Notícias</span></a></li>
								<li><a href="reportagens" title="Reportagens" @if(str_is('textos.reportagens*', Route::currentRouteName())) class="ativo" @endif><span>Reportagens</span></a></li>
								<li><a href="entrevistas" title="Entrevistas" @if(str_is('textos.entrevistas*', Route::currentRouteName())) class="ativo" @endif><span>Entrevistas</span></a></li>
							</ul>
						</li>
						<li @if(str_is('textos.eventos*', Route::currentRouteName())) class="ativo" @endif><a href="eventos" title="Eventos" class="linkMnEventos @if(str_is('textos.eventos*', Route::currentRouteName())) ativo @endif">EVENTOS</a></li>
						<li class="comsub @if(str_is('textos.artigos*', Route::currentRouteName()) || str_is('textos.dicas*', Route::currentRouteName()) || str_is('textos.pesquisas*', Route::currentRouteName())) ativo @endif">
							<a href="#" title="Carreira" class="linkMnCarreira @if(str_is('textos.artigos*', Route::currentRouteName()) || str_is('textos.dicas*', Route::currentRouteName()) || str_is('textos.pesquisas*', Route::currentRouteName())) ativo @endif">CARREIRA <span class="caret"></span></a>
							<ul class="sub">
								<li><a href="artigos" title="Artigos" @if(str_is('textos.artigos*', Route::currentRouteName())) class="ativo" @endif><span>Artigos</span></a></li>
								<li><a href="dicas" title="Dicas" @if(str_is('textos.dicas*', Route::currentRouteName())) class="ativo" @endif><span>Dicas</span></a></li>
								<li><a href="pesquisas" title="Pesquisas" @if(str_is('textos.pesquisas*', Route::currentRouteName())) class="ativo" @endif><span>Pesquisas</span></a></li>
							</ul>
						</li>
						<li @if(str_is('enquetes*', Route::currentRouteName())) class="ativo" @endif><a href="enquetes" title="Enquetes" class="linkMnEnquetes @if(str_is('enquetes*', Route::currentRouteName())) ativo @endif">ENQUETES</a></li>
					</ul>
				</nav>
				<div id="toggleMenuMobile" class="hide-desktop hide-tablet">
					<a href="#" title="Abrir Menu"><span>MENU</span></a>
				</div>
				<nav id="menuMobile" class="hide-desktop hide-tablet">
					<ul class="pure-g">
						<li class="pure-u-1-2"><a href="home" title="Página Inicial" @if(str_is('home', Route::currentRouteName())) class="ativo" @endif>HOME</a></li>
						<li class="pure-u-1-2"><a href="quem-somos" title="Quem Somos" @if(str_is('quemsomos', Route::currentRouteName())) class="ativo" @endif>QUEM SOMOS</a></li>
						<li class="pure-u-1-2"><a href="equipe" title="Equipe Meta News" @if(str_is('equipe', Route::currentRouteName())) class="ativo" @endif>EQUIPE</a></li>
						<li class="pure-u-1-2"><a href="imprensa" title="Imprensa" @if(str_is('imprensa', Route::currentRouteName())) class="ativo" @endif>IMPRENSA</a></li>
						<li class="pure-u-1-2"><a href="perguntas-frequentes" title="Perguntas Frequentes" @if(str_is('perguntasfrequentes', Route::currentRouteName())) class="ativo" @endif>PERGUNTAS FREQUENTES</a></li>
						<li class="pure-u-1-2"><a href="contato" title="Contato" @if(str_is('contato', Route::currentRouteName())) class="ativo" @endif>CONTATO</a></li>
						<li class="pure-u-1-2"><a href="noticias" title="Notícias" class="linkMnNoticias @if(str_is('textos.noticias*', Route::currentRouteName())) ativo @endif">NOTÍCIAS</a></li>
						<li class="pure-u-1-2"><a href="reportagens" title="Reportagens" class="linkMnNoticias @if(str_is('textos.reportagens*', Route::currentRouteName())) ativo @endif">REPORTAGENS</a></li>
						<li class="pure-u-1-2"><a href="entrevistas" title="Notícias" class="linkMnNoticias @if(str_is('textos.entrevistas*', Route::currentRouteName())) ativo @endif">ENTREVISTAS</a></li>
						<li class="pure-u-1-2"><a href="eventos" title="Eventos" class="linkMnEventos @if(str_is('textos.eventos*', Route::currentRouteName())) ativo @endif">EVENTOS</a></li>
						<li class="pure-u-1-2"><a href="artigos" title="Artigos" @if(str_is('textos.artigos*', Route::currentRouteName())) class="ativo" @endif><span>Artigos</span></a></li>
						<li class="pure-u-1-2"><a href="dicas" title="Dicas" @if(str_is('textos.icas*', Route::currentRouteName())) class="ativo" @endif><span>Dicas</span></a></li>
						<li class="pure-u-1-2"><a href="pesquisas" title="Pesquisas" @if(str_is('textos.pesquisas*', Route::currentRouteName())) class="ativo" @endif><span>Pesquisas</span></a></li>
						<li class="pure-u-1-2"><a href="enquetes" title="Enquetes" class="linkMnEnquetes @if(str_is('enquetes*', Route::currentRouteName())) ativo @endif">ENQUETES</a></li>
					</ul>
				</nav>
			</div>
		</div>

		@if(isset($pagetitle))
			<h1 id="pagetitle">
				<div class="centro">
					{{$pagetitle}}
				</div>
			</h1>
		@endif

	</header>

	<!-- Conteúdo Principal -->
	<div class="main centro {{ 'main-'. str_replace('-', '', Route::currentRouteName()) }}">
		@yield('conteudo')

		@if(count($bannerInferior))
			<div id="bannerInferior">
				@foreach($bannerInferior as $banner)
					<a href="{{$banner->link}}" target="_blank">
						<img src="assets/images/bannerinferior/{{$banner->imagem}}">
					</a>
				@endforeach
		</div>
		@endif

	</div>

	<footer>
		<div class="centro">
			<nav>
				<ul class="superior">
					<li><a href="home" title="Home">HOME</a></li>
					<li><a href="quem-somos" title="Quem Somos">QUEM SOMOS</a></li>
					<li><a href="equipe" title="Equipe">EQUIPE</a></li>
					<li><a href="imprensa" title="Imprensa">IMPRENSA</a></li>
					<li><a href="perguntas-frequentes" title="Perguntas Frequentes">PERGUNTAS FREQUENTES</a></li>
					<li><a href="contato" title="Contato">CONTATO</a></li>
				</ul>
				<ul class="inferior">
					<li><a href="noticias" title="Notícias">NOTÍCIAS</a></li>
					<li><a href="reportagens" title="Notícias">REPORTAGENS</a></li>
					<li><a href="entrevistas" title="Notícias">ENTREVISTAS</a></li>
					<li><a href="eventos" title="Eventos">EVENTOS</a></li>
					<li><a href="artigos" title="Artigos">ARTIGOS</a></li>
					<li><a href="dicas" title="Dicas">DICAS</a></li>
					<li><a href="pesquisas" title="Pesquisas">PESQUISAS</a></li>
					<li><a href="enquetes" title="Enquetes">ENQUETES</a></li>
				</ul>
			</nav>

			<div class="assinatura">
				<div class="telefone">
					<span>{{$contatoLayout->prefixo_telefone}}</span> {{$contatoLayout->telefone}}
				</div>
				<div class="email">
					<a href="mailto:{{$contatoLayout->email}}" title="Entre em contato">{{$contatoLayout->email}}</a>
				</div>
				<div class="privacy">
					<a href="politica-de-privacidade" title="Política de Privacidade">política de privacidade</a>
				</div>
				<div class="copyright">
					&copy; {{Date('Y')}} metaNEWS. Todos os direitos reservados.
				</div>
				<div class="trupe">
					Criação de sites: <a href="http://www.trupe.net" target="_blank" title="Criação de sites: Trupe Agência Criativa">Trupe Agência Criativa <img src="assets/images/layout/trupe.png" alt="Criação de sites Trupe Agência Criativa"></a>
				</div>
			</div>
		</div>
	</footer>

	<?=Assets::JS(array(
		'vendor/jquery-colorbox/jquery.colorbox-min',
		'vendor/jquery.cycle/jquery.cycle.all',
		'vendor/screwdefaultbuttons/js/jquery.screwdefaultbuttonsV2',
		'vendor/imagesloaded/imagesloaded.pkgd.min',
		'vendor/masonry/dist/masonry.pkgd.min',
		'vendor/fancybox/source/jquery.fancybox',
		'js/main'
	))?>

	@if(Session::has('alerta'))
		<script defer>
			$.colorbox({
				close : 'fechar',
				closeButton	: true,
				html : "<div class='pedirEmail alg-c'><h1>{{Session::get('alerta')}}</h1></div>",
				scrolling : false,
				width:770,
				height:180
			});
		</script>
	@endif

<!-- Código do Google para tag de remarketing -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 931749432;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/931749432/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

@if(1 == 2 && str_is('home', Route::currentRouteName()))
	<script defer>
		$('document').ready( function(){
			$.fancybox.open([
				{
					href : 'assets/images/popup-premio-GrupoMeta.png',
				}
			]);
		});
	</script>
@endif

</body>
</html>
