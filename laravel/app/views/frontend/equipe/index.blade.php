@section('conteudo')
	
	@if(sizeof($equipe))
		<div id="listaEquipe">
			<?php $contador = 0 ?>
			@foreach($equipe as $membro)
				<div class="contemEquipe">
					<div class="equipe pad-{{$contador}}">
						<h1>{{$membro->nome}}</h1>
						<h2>{{$membro->cargo}}</h2>
						<div class="email">
							<a href="mailto:{{$membro->email}}" title="Email">{{$membro->email}}</a>
						</div>
						<div class="cke">
							{{$membro->texto}}
						</div>
					</div>
				</div>
				<?php 
					$contador++;
					if($contador == 3) $contador = 0;
				?>
			@endforeach
		</div>
	@endif

@stop
