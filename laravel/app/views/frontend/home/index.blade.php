@section('conteudo')

	<div class="pure-g">

		<div class="pure-u-2-3 pure-u-mobile-1 pure-u-tablet-1">
			<div id="banners">
				<div class="animate">
					@if(sizeof($banners))
						@foreach ($banners as $key => $value)
							<a href="{{$value->link}}" title="{{$value->texto}}" class="banner">
								<img src="assets/images/banners/{{$value->imagem}}" alt="{{$value->texto}}">
								<div class="texto">{{$value->texto}}</div>
							</a>
						@endforeach
					@endif
				</div>
				<div id="controls"></div>
			</div>
		</div>

		<div class="pure-u-1-3 pure-u-mobile-1 pure-u-tablet-1">
			<div id="enquete">
				@if(isset($enquete->texto))
					<h1>ENQUETE <a href="enquetes" title="ver enquetes anteriores">ver anteriores &raquo;</a></h1>
					<div class="corpo">
						<div class="questao">{{$enquete->texto}}</div>
						
						@if(Session::has('enqueteVotada') && Session::get('enqueteVotada') == $enquete->id)
							<div class="resultados">
								@if($enquete->opcoes)							
									@foreach($enquete->opcoes as $opcao)
										<div class="porcentagem">
											<div class="barra"><div style="width:{{EnquetesHelper::porcentagemDeVotos($opcao->id)}}"></div></div>
											<div class="texto">
												<span class="porcent">{{EnquetesHelper::porcentagemDeVotos($opcao->id)}}</span> <span class="separador">|</span> {{$opcao->texto}}
											</div>
										</div>
									@endforeach
								@endif
							</div>
						@else
							<form action="computarVoto" method="post">
								<div class="opcoes">
									@if($enquete->opcoes)							
										@foreach($enquete->opcoes as $opcao)
											<label>
												<input type="radio" name="respostaEnquete" value="{{$opcao->id}}"> {{$opcao->texto}}
											</label>											
										@endforeach
									@endif
								</div>
								<div class="submit">
									<input type="submit" value="VOTAR"> <a href="enquetes/resultados/{{Tools::dia($enquete->data)}}/{{Tools::mes($enquete->data, false)}}/{{Tools::ano($enquete->data)}}/{{$enquete->id}}" title="ver resultados">ver resultados &raquo;</a>
								</div>
							</form>						
						@endif
					</div>
				@endif
			</div>
		</div>
		
	</div>

	@if(sizeof($destaques))
		<div id="destaques" class="pure-g">
			@foreach($destaques as $i => $destaque)
				<div class="pure-u-1-2 pure-u-mobile-1 pure-u-tablet-1-2">
					<div class="pad-tablet-{{$i}}">
						<a href="{{$destaque->categoria->slug}}/{{$destaque->slug}}" title="{{$destaque->titulo}}" class="chamada chamada-{{$i}}">
							@if($destaque->capa)
								<img src="assets/images/textos/capas/destaque_home/{{$destaque->capa}}" alt="{{$destaque->titulo}}">
							@else
								<img src="http://www.placehold.it/470x300/EFEFEF/AAAAAA&text=Nenhuma+imagem">
							@endif
							@if($destaque->video_destaque)
								<div class="play"></div>
							@endif
							<div class="overlay">
								<div class="linha">
									<div class="labelCategoria">{{$destaque->categoria->titulo}}</div>
									<div class="float-r">
										<div class="data">{{Tools::converteDataFrontend($destaque->data_publicacao)}}</div>
										<div class="comentarios">{{Tools::numeroComentarios($destaque->id)}}</div>
									</div>
								</div>
								<div class="texto">{{$destaque->titulo}}</div>
							</div>
						</a>
					</div>
				</div>
			@endforeach
		</div>
	@endif

	<div id="listaLinks" class="pure-g">

		<div class="pure-u-2-3 pure-u-mobile-1 pure-u-tablet-1">

			<div class="pad-r">
			
				@if(isset($textos[0]))
					<a href="{{$textos[0]->categoria->slug}}/{{$textos[0]->slug}}" title="{{$textos[0]->titulo}}" class="link-grande @if($textos[0]->capa) comimagem @endif">
						@if($textos[0]->capa)
							<div class="capa">
								<img src="assets/images/textos/capas/grande/{{$textos[0]->capa}}" alt="{{$textos[0]->titulo}}">
								@if($textos[0]->video_destaque)
									<div class="play"></div>
								@endif
							</div>
						@endif
						<div class="miolo">
							<div class="labelCategoria @if(!$textos[0]->capa && $textos[0]->video_destaque) complay @endif">{{$textos[0]->categoria->titulo}}</div>
							<div class="barra-superior">
								<div class="data">{{Tools::converteDataFrontend($textos[0]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[0]->id)}}</div>
							</div>
							<h1>{{$textos[0]->titulo}}</h1>
							<h2>{{$textos[0]->olho}}</h2>
							<div class="barra-inferior">
								<div class="data">{{Tools::converteDataFrontend($textos[0]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[0]->id)}}</div>
							</div>
						</div>
					</a>
				@endif

				@if(isset($textos[1]))
					<a href="{{$textos[1]->categoria->slug}}/{{$textos[1]->slug}}" title="{{$textos[1]->titulo}}" class="link-grande @if($textos[1]->capa) comimagem @endif">
						@if($textos[1]->capa)
							<div class="capa">
								<img src="assets/images/textos/capas/grande/{{$textos[1]->capa}}" alt="{{$textos[1]->titulo}}">
								@if($textos[1]->video_destaque)
									<div class="play"></div>
								@endif
							</div>
						@endif
						<div class="miolo">
							<div class="labelCategoria @if(!$textos[1]->capa && $textos[1]->video_destaque) complay @endif">{{$textos[1]->categoria->titulo}}</div>
							<div class="barra-superior">
								<div class="data">{{Tools::converteDataFrontend($textos[1]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[1]->id)}}</div>
							</div>
							<h1>{{$textos[1]->titulo}}</h1>
							<h2>{{$textos[1]->olho}}</h2>
							<div class="barra-inferior">
								<div class="data">{{Tools::converteDataFrontend($textos[1]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[1]->id)}}</div>
							</div>
						</div>
					</a>
				@endif

				<div class="pure-g">
					
					<div class="pure-u-1-2 pure-u-mobile-1">
						<div class="pad-r">
							@if(isset($textos[2]))
								<a href="{{$textos[2]->categoria->slug}}/{{$textos[2]->slug}}" title="{{$textos[2]->titulo}}" class="link-medio @if($textos[2]->capa) comimagem @endif">
									@if($textos[2]->capa)
										<div class="capa">
											<img src="assets/images/textos/capas/pequena/{{$textos[2]->capa}}" alt="{{$textos[2]->titulo}}">
											@if($textos[2]->video_destaque)
												<div class="play"></div>
											@endif
										</div>
									@endif
									<div class="miolo">
										<div class="labelCategoria @if(!$textos[2]->capa && $textos[2]->video_destaque) complay @endif">{{$textos[2]->categoria->titulo}}</div>
										<div class="barra-superior">
											<div class="data">{{Tools::converteDataFrontend($textos[2]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[2]->id)}}</div>
										</div>
										<h1>{{$textos[2]->titulo}}</h1>
										<h2>{{$textos[2]->olho}}</h2>
										<div class="barra-inferior">
											<div class="data">{{Tools::converteDataFrontend($textos[2]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[2]->id)}}</div>
										</div>
									</div>
								</a>
							@endif

							@if(isset($textos[3]))
								<a href="{{$textos[3]->categoria->slug}}/{{$textos[3]->slug}}" title="{{$textos[3]->titulo}}" class="link-medio @if($textos[3]->capa) comimagem @endif">
									@if($textos[3]->capa)
										<div class="capa">
											<img src="assets/images/textos/capas/pequena/{{$textos[3]->capa}}" alt="{{$textos[3]->titulo}}">
											@if($textos[3]->video_destaque)
												<div class="play"></div>
											@endif
										</div>
									@endif
									<div class="miolo">
										<div class="labelCategoria @if(!$textos[3]->capa && $textos[3]->video_destaque) complay @endif">{{$textos[3]->categoria->titulo}}</div>
										<div class="barra-superior">
											<div class="data">{{Tools::converteDataFrontend($textos[3]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[3]->id)}}</div>
										</div>
										<h1>{{$textos[3]->titulo}}</h1>
										<h2>{{$textos[3]->olho}}</h2>
										<div class="barra-inferior">
											<div class="data">{{Tools::converteDataFrontend($textos[3]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[3]->id)}}</div>
										</div>
									</div>
								</a>
							@endif
						</div>
					</div>

					<div class="pure-u-1-2 pure-u-mobile-1">
						<div class="pad-l">
							@if(isset($textos[4]))
								<a href="{{$textos[4]->categoria->slug}}/{{$textos[4]->slug}}" title="{{$textos[4]->titulo}}" class="link-medio @if($textos[4]->capa) comimagem @endif">
									@if($textos[4]->capa)
										<div class="capa">
											<img src="assets/images/textos/capas/pequena/{{$textos[4]->capa}}" alt="{{$textos[4]->titulo}}">
											@if($textos[4]->video_destaque)
												<div class="play"></div>
											@endif
										</div>
									@endif
									<div class="miolo">
										<div class="labelCategoria @if(!$textos[4]->capa && $textos[4]->video_destaque) complay @endif">{{$textos[4]->categoria->titulo}}</div>
										<div class="barra-superior">
											<div class="data">{{Tools::converteDataFrontend($textos[4]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[4]->id)}}</div>
										</div>
										<h1>{{$textos[4]->titulo}}</h1>
										<h2>{{$textos[4]->olho}}</h2>
										<div class="barra-inferior">
											<div class="data">{{Tools::converteDataFrontend($textos[4]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[4]->id)}}</div>
										</div>
									</div>
								</a>
							@endif

							@if(isset($textos[5]))
								<a href="{{$textos[5]->categoria->slug}}/{{$textos[5]->slug}}" title="{{$textos[5]->titulo}}" class="link-medio @if($textos[5]->capa) comimagem @endif">
									@if($textos[5]->capa)
										<div class="capa">
											<img src="assets/images/textos/capas/pequena/{{$textos[5]->capa}}" alt="{{$textos[5]->titulo}}">
											@if($textos[5]->video_destaque)
												<div class="play"></div>
											@endif
										</div>
									@endif
									<div class="miolo">
										<div class="labelCategoria @if(!$textos[5]->capa && $textos[5]->video_destaque) complay @endif">{{$textos[5]->categoria->titulo}}</div>
										<div class="barra-superior">
											<div class="data">{{Tools::converteDataFrontend($textos[5]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[5]->id)}}</div>
										</div>
										<h1>{{$textos[5]->titulo}}</h1>
										<h2>{{$textos[5]->olho}}</h2>
										<div class="barra-inferior">
											<div class="data">{{Tools::converteDataFrontend($textos[5]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[5]->id)}}</div>
										</div>
									</div>
								</a>
							@endif
						</div>
					</div>

				</div>

			</div>

		</div>

		<div class="pure-u-1-3 pure-u-mobile-1 pure-u-tablet-1">

			<div class="pad-l pure-g">

				@if(isset($textos[6]))
					<a href="{{$textos[6]->categoria->slug}}/{{$textos[6]->slug}}" title="{{$textos[6]->titulo}}" class="link-pequeno pure-u-1 pure-u-tablet-1-2">
						@if($textos[6]->capa)
							<div class="capa">
								<img src="assets/images/textos/capas/pequena/{{$textos[6]->capa}}" alt="{{$textos[6]->titulo}}">
								@if($textos[6]->video_destaque)
									<div class="play"></div>
								@endif
							</div>
						@endif
						<div class="miolo">
							<div class="labelCategoria @if(!$textos[6]->capa && $textos[6]->video_destaque) complay @endif">{{$textos[6]->categoria->titulo}}</div>
							<div class="barra-superior">
								<div class="data">{{Tools::converteDataFrontend($textos[6]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[6]->id)}}</div>
							</div>
							<h1>{{$textos[6]->titulo}}</h1>
							<h2>{{$textos[6]->olho}}</h2>
							<div class="barra-inferior">
								<div class="data">{{Tools::converteDataFrontend($textos[6]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[6]->id)}}</div>
							</div>
						</div>
					</a>
				@endif

				@if(isset($textos[7]))
					<a href="{{$textos[7]->categoria->slug}}/{{$textos[7]->slug}}" title="{{$textos[7]->titulo}}" class="link-pequeno pure-u-1 pure-u-tablet-1-2">
						@if($textos[7]->capa)
							<div class="capa">
								<img src="assets/images/textos/capas/pequena/{{$textos[7]->capa}}" alt="{{$textos[7]->titulo}}">
								@if($textos[7]->video_destaque)
									<div class="play"></div>
								@endif
							</div>
						@endif
						<div class="miolo">
							<div class="labelCategoria @if(!$textos[7]->capa && $textos[7]->video_destaque) complay @endif">{{$textos[7]->categoria->titulo}}</div>
							<div class="barra-superior">
								<div class="data">{{Tools::converteDataFrontend($textos[7]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[7]->id)}}</div>
							</div>
							<h1>{{$textos[7]->titulo}}</h1>
							<h2>{{$textos[7]->olho}}</h2>
							<div class="barra-inferior">
								<div class="data">{{Tools::converteDataFrontend($textos[7]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[7]->id)}}</div>
							</div>
						</div>
					</a>
				@endif

				@if(isset($textos[8]))
					<a href="{{$textos[8]->categoria->slug}}/{{$textos[8]->slug}}" title="{{$textos[8]->titulo}}" class="link-pequeno pure-u-1 pure-u-tablet-1-2">
						@if($textos[8]->capa)
							<div class="capa">
								<img src="assets/images/textos/capas/pequena/{{$textos[8]->capa}}" alt="{{$textos[8]->titulo}}">
								@if($textos[8]->video_destaque)
									<div class="play"></div>
								@endif
							</div>
						@endif
						<div class="miolo">
							<div class="labelCategoria @if(!$textos[8]->capa && $textos[8]->video_destaque) complay @endif">{{$textos[8]->categoria->titulo}}</div>
							<div class="barra-superior">
								<div class="data">{{Tools::converteDataFrontend($textos[8]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[8]->id)}}</div>
							</div>
							<h1>{{$textos[8]->titulo}}</h1>
							<h2>{{$textos[8]->olho}}</h2>
							<div class="barra-inferior">
								<div class="data">{{Tools::converteDataFrontend($textos[8]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[8]->id)}}</div>
							</div>
						</div>
					</a>
				@endif

				@if(isset($textos[9]))
					<a href="{{$textos[9]->categoria->slug}}/{{$textos[9]->slug}}" title="{{$textos[9]->titulo}}" class="link-pequeno pure-u-1 pure-u-tablet-1-2">
						@if($textos[9]->capa)
							<div class="capa">
								<img src="assets/images/textos/capas/pequena/{{$textos[9]->capa}}" alt="{{$textos[9]->titulo}}">
								@if($textos[9]->video_destaque)
									<div class="play"></div>
								@endif
							</div>
						@endif
						<div class="miolo">
							<div class="labelCategoria @if(!$textos[9]->capa && $textos[9]->video_destaque) complay @endif">{{$textos[9]->categoria->titulo}}</div>
							<div class="barra-superior">
								<div class="data">{{Tools::converteDataFrontend($textos[9]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[9]->id)}}</div>
							</div>
							<h1>{{$textos[9]->titulo}}</h1>
							<h2>{{$textos[9]->olho}}</h2>
							<div class="barra-inferior">
								<div class="data">{{Tools::converteDataFrontend($textos[9]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[9]->id)}}</div>
							</div>
						</div>
					</a>
				@endif

				@if(isset($textos[10]))
					<a href="{{$textos[10]->categoria->slug}}/{{$textos[10]->slug}}" title="{{$textos[10]->titulo}}" class="link-pequeno pure-u-1 pure-u-tablet-1-2">
						@if($textos[10]->capa)
							<div class="capa">
								<img src="assets/images/textos/capas/pequena/{{$textos[10]->capa}}" alt="{{$textos[10]->titulo}}">
								@if($textos[10]->video_destaque)
									<div class="play"></div>
								@endif
							</div>
						@endif
						<div class="miolo">
							<div class="labelCategoria @if(!$textos[10]->capa && $textos[10]->video_destaque) complay @endif">{{$textos[10]->categoria->titulo}}</div>
							<div class="barra-superior">
								<div class="data">{{Tools::converteDataFrontend($textos[10]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[10]->id)}}</div>
							</div>
							<h1>{{$textos[10]->titulo}}</h1>
							<h2>{{$textos[10]->olho}}</h2>
							<div class="barra-inferior">
								<div class="data">{{Tools::converteDataFrontend($textos[10]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[10]->id)}}</div>
							</div>
						</div>
					</a>
				@endif

				<div id="formNewsletter" class="pure-u-1 pure-u-tablet-1-2">
					<h1 class="pure-u-1">RECEBA NOSSAS NOVIDADES POR E-MAIL</h1>
					<form action="cadastroNewsletter" method="post" class="pure-g">
						<div class="pure-u-7-8">
							<input type="text" name="newsletterNome" placeholder="Nome" required>
							<input type="email" name="newsletterEmail" placeholder="E-mail" required>
						</div>
						<div class="pure-u-1-8">
							<input type="submit" value="&raquo;">
						</div>
					</form>
				</div>

			</div>

		</div>
		
	</div>

@stop