@section('conteudo')
	
	<div class="pure-g">
		
		<div class="pure-u-1-3 pure-u-mobile-1">
			<div class="telefone">
				<span>{{$contato->prefixo_telefone}}</span> {{$contato->telefone}}
			</div>
			<div class="email">
				<a href="mailto:{{$contato->email}}" title="Entre em contato">{{$contato->email}}</a>
			</div>
		</div>

		<div class="pure-u-2-3 pure-u-mobile-1">
			<div class="cke">
				{{$contato->texto}}
			</div>	
			
			<form action="{{URL::route('enviarContato')}}" method="post" class="pure-g" novalidate>
				
				<div class="pure-u-5-12 pure-u-mobile-1">
					<div class="pad">
						
						<select name="contatoAssunto" id="assuntoContato" required>
							<option value="">Assunto</option>
							<option value="Sugestões" @if(Session::has('formContato') && Session::get('formContato.assunto') == 'Sugestões') selected @endif>Sugestões</option>
							<option value="Reclamações" @if(Session::has('formContato') && Session::get('formContato.assunto') == 'Reclamações') selected @endif>Reclamações</option>
							<option value="Dúvidas" @if(Session::has('formContato') && Session::get('formContato.assunto') == 'Dúvidas') selected @endif>Dúvidas</option>
						</select>

						<input type="text" name="contatoNome" placeholder="Nome" required @if(Session::has('formContato')) value="{{Session::get('formContato.nome')}}" @endif>

						<input type="email" name="contatoEmail" placeholder="E-mail" required @if(Session::has('formContato')) value="{{Session::get('formContato.email')}}" @endif>

						<input type="text" name="contatoTelefone" placeholder="Telefone" @if(Session::has('formContato')) value="{{Session::get('formContato.telefone')}}" @endif>

					</div>
				</div>

				<div class="pure-u-7-12 pure-u-mobile-1">
					<textarea name="contatoMensagem" placeholder="Mensagem" required>@if(Session::has('formContato')) {{Session::get('formContato.mensagem')}} @endif</textarea>
				</div>

				<div class="pure-u-1">
					<div class="submit">
						<input type="submit" value="enviar &raquo;">
					</div>
				</div>
			</form>
			
		</div>

	</div>

@stop
