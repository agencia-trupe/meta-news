@section('conteudo')

	<section class="perguntasfrequentes">
		@if(count($semCategoria))
		<div style="margin-bottom:30px;">
			@foreach($semCategoria as $pergunta)
			<div class="pergunta">
				<span class="status"></span>
				<a href="#">{{ $pergunta->pergunta }}</a>

				<div class="resposta">
					{{ $pergunta->resposta }}
				</div>
			</div>
			@endforeach
		</div>
		@endif

		@foreach($registros as $registro)
		@if(count($registro->perguntas))
		<h2>{{ $registro->titulo }}</h2>
			@foreach($registro->perguntas as $pergunta)
			<div class="pergunta">
				<span class="status"></span>
				<a href="#">{{ $pergunta->pergunta }}</a>

				<div class="resposta">
					{{ $pergunta->resposta }}
				</div>
			</div>
			@endforeach
		@endif
		@endforeach
	</section>

	<aside class="perguntasfrequentes-imagem">
		<img src="assets/images/perguntasfrequentes/foto.jpg" alt="Perguntas Frequentes">
	</aside>

@stop
