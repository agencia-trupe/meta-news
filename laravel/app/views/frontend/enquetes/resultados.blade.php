@section('conteudo')

	<div id="mostraEnquete" class="pure-g">

		<div class="pure-u-1-5 pure-u-mobile-1">
			<h1>ENQUETE <strong>#{{$detalhe->id}}</strong></h1>

			<div class="data">
				<div class="mes">{{Tools::mes($detalhe->data)}}</div>
				<div class="ano">{{Tools::ano($detalhe->data)}}</div>
			</div>
		</div>

		<div class="pure-u-4-5 pure-u-mobile-1">

			<div class="pad">
				
				<h2>{{$detalhe->texto}}</h2>
			
				<div class="opcoes">
					@if($detalhe->opcoes)							
						@foreach($detalhe->opcoes as $opcao)
							<div class="porcentagem">
								<div class="barra"><div style="width:{{EnquetesHelper::porcentagemDeVotos($opcao->id)}}"></div></div>
								<div class="texto">
									<span class="porcent">{{EnquetesHelper::porcentagemDeVotos($opcao->id)}}</span> <span class="separador">|</span> {{$opcao->texto}}
								</div>
							</div>
						@endforeach
					@endif
				</div>
				
			</div>

		</div>

		<div class="pure-u-1">
			<a id="backbutton" href="{{URL::previous()}}" title="&laquo; voltar">&laquo; voltar</a>
		</div>
		
	</div>

@stop
