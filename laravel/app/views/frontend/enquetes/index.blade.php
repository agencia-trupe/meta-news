@section('conteudo')
	
	<div id="mostraEnquete" class="pure-g">

		@if(isset($ativa->id))

			<div class="pure-u-1-5 pure-u-mobile-1">
				<h1>ENQUETE <strong>#{{$ativa->id}}</strong></h1>

				<div class="data">
					<div class="mes">{{Tools::mes($ativa->data)}}</div>
					<div class="ano">{{Tools::ano($ativa->data)}}</div>
				</div>
			</div>

			<div class="pure-u-4-5 pure-u-mobile-1" id="enquete">

				<div class="pad">
					
					<h2>{{$ativa->texto}}</h2>

					@if(Session::has('enqueteVotada') && Session::get('enqueteVotada') == $ativa->id)
						<div class="resultados">
							<div class="opcoes">
							@if($ativa->opcoes)							
								@foreach($ativa->opcoes as $opcao)
									<div class="porcentagem">
										<div class="barra"><div style="width:{{EnquetesHelper::porcentagemDeVotos($opcao->id)}}"></div></div>
										<div class="texto">
											<span>{{EnquetesHelper::porcentagemDeVotos($opcao->id)}}</span> {{$opcao->texto}}
										</div>
									</div>
								@endforeach
							@endif
							</div>
						</div>					
					@else
						<form action="computarVoto" method="post">
							<div class="opcoes">
								@if($ativa->opcoes)							
									@foreach($ativa->opcoes as $opcao)
										<label>
											<input type="radio" name="respostaEnquete" value="{{$opcao->id}}"> {{$opcao->texto}}
										</label>
									@endforeach
								@endif
							</div>
							<div class="submit">
								<input type="submit" value="VOTAR"> <a href="enquetes/resultados/{{Tools::dia($ativa->data)}}/{{Tools::mes($ativa->data, false)}}/{{Tools::ano($ativa->data)}}/{{$ativa->id}}" title="Ver resultados">ver resultados &raquo;</a>
							</div>
						</form>
					@endif

				</div>

			</div>

		@endif
		
	</div>

	@if($historico)
		<div id="listaHistorico" class="pure-g">		
			@foreach($historico as $enquete)
				<div class="pure-u-1-2 pure-u-mobile-1">
					<div class="pad">
						<a href="enquetes/resultados/{{Tools::dia($enquete->data)}}/{{Tools::mes($enquete->data, false)}}/{{Tools::ano($enquete->data)}}/{{$enquete->id}}" title="Ver enquete passada">
							<div class="pure-g">
								<div class="data pure-u-1-6">{{Tools::dataEnquete($enquete->data)}}</div>
								<h2 class="pure-u-5-6">{{$enquete->texto}}</h2>
							</div>
						</a>
					</div>
				</div>
			@endforeach			
		</div>
		<div id="paginacaoEnquete">{{$historico->links()}}</div>
	@endif

@stop
