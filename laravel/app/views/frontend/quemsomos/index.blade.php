@section('conteudo')
	
	<div class="pure-g">

		<section class="pure-u-7-12 pure-u-mobile-1">
			<div class="cke">
				{{$texto->texto}}
			</div>
		</section>

		<aside class="pure-u-5-12 pure-u-mobile-1">
			<img src="assets/images/quemsomos/{{$texto->imagem}}" alt="Quem Somos">
			<a href="equipe" title="conheça nossa equipe">conheça nossa equipe &raquo;</a>
		</aside>
		
	</div>

@stop
