@section('conteudo')
	
	<div class="pure-g">

		<section class="pure-u-1-2 pure-u-mobile-1">
			<div class="texto-imprensa">
				<h2>Assessoria de Comunicação</h2>
				<div class="cke">
					{{ $texto->assessoria }}
				</div>				
			</div>
		</section>

		<aside class="pure-u-1-2 pure-u-mobile-1">
			<img src="assets/images/quemsomos/{{ $texto->imagem }}" class="imagem-imprensa" alt="Imprensa">			
		</aside>
		
	</div>

@stop