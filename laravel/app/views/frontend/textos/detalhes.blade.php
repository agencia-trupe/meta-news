@section('conteudo')

	<div class="pure-g" id="detalhes">
		
		<section class="pure-u-2-3 pure-u-mobile-1">
			<div class="pad">

				<h1>{{$texto->titulo}}</h1>
				<div id="idtexto" style="display:none;">{{$texto->id}}</div>

				@if($texto->imagem_destaque)
					<img id="imagem-destaque" src="assets/images/textos/imagem_destaque/redimensionada/{{$texto->imagem_destaque}}" alt="{{$texto->titulo}}">
				@endif

				@if($texto->video_destaque)
					<div id="video-destaque">
						{{Tools::embed($texto->video_destaque, '100%')}}
					</div>
				@endif
				
				<div class="olho">{{$texto->olho}}</div>

				<div class="barra">
					<div class="categoria">{{$texto->categoria->titulo}}</div>
					<div class="data">{{Tools::converteDataFrontend($texto->data_publicacao)}}</div>
					<div class="comentarios">{{Tools::numeroComentarios($texto->id)}}</div>
					<div class="social">
						<div class="fb">
							<div class="fb-like" data-href="{{Request::url().'?nocache=true'}}" data-width="180" data-layout="standard" data-action="like" data-show-faces="false" data-share="true"></div>
						</div>
						<a href="https://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a>
						<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
						<script src="//platform.linkedin.com/in.js" type="text/javascript">lang: pt_BR</script><script type="IN/Share" data-url="{{ Request::url() }}"></script>
						<a href="#" class="link-enviar" title="Enviar para um amigo"><img src="assets/images/layout/botao.png" alt="Enviar para um amigo"></a>
					</div>
				</div>
				
				@if($texto->texto)
					<div class="texto cke">
						{{\Tools::corrigePathImagens($texto->texto)}}
					</div>
					@if(sizeof($texto->imagens))
						<div id="galeria">
							<h2>GALERIA DE FOTOS</h2>
							<?php $cont = 0 ?>
							<div class="contemImagens">
								@foreach($texto->imagens as $img)
									<?php $cont++ ?>
									<div class="link-galeria">
										<a href="assets/images/textosimagens/{{$img->imagem}}" rel="galeria" class="shadow">
											<img src="assets/images/textosimagens/thumbs/{{$img->imagem}}">
											<div class="overlay"></div>
										</a>
									</div>
									<?php if($cont == 5) $cont=0; ?>
								@endforeach
							</div>
						</div>
					@endif
					<div class="barra-social-inferior">
						<div class="fb">
							<div class="fb-like" data-href="{{Request::url()}}" data-width="180" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
						</div>
						<a href="https://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a>
						<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
						<script src="//platform.linkedin.com/in.js" type="text/javascript">lang: pt_BR</script><script type="IN/Share" data-url="{{ Request::url() }}"></script>
						<a href="#" class="link-enviar" title="Enviar para um amigo"><img src="assets/images/layout/botao.png" alt="Enviar para um amigo"></a>
					</div>
				@endif

				<div id="comentarios">
					<div class="fb-comments" data-href="{{Request::url()}}" data-width="100%" data-numposts="5" data-colorscheme="light"></div>
				</div>

			</div>
		</section>

		<aside class="pure-u-1-3 pure-u-mobile-1">
			<div class="pad">
				<h2>LEIA TAMBÉM</h2>
				@if($relacionados)
					@foreach($relacionados as $link)						
						<a href="{{$link->categoria->slug}}/{{$link->slug}}" title="{{$link->titulo}}" class="link-pequeno @if($link->capa) comimagem @endif">
							@if($link->capa)
								<div class="capa">
									<img src="assets/images/textos/capas/pequena/{{$link->capa}}" alt="{{$link->titulo}}">
									@if($link->video_destaque)
										<div class="play"></div>
									@endif
								</div>
							@endif
							<div class="miolo">
								<div class="barra-superior">
									<div class="data">{{Tools::converteDataFrontend($link->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($link->id)}}</div>
								</div>
								<h1>{{$link->titulo}}</h1>
								<h2>{{$link->olho}}</h2>
								<div class="barra-inferior">
									<div class="data">{{Tools::converteDataFrontend($link->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($link->id)}}</div>
								</div>
							</div>
						</a>
					@endforeach
				@endif
			</div>
		</aside>

		<div class="pure-u-1">
			<a id="backbutton" href="{{URL::to($texto->categoria->slug)}}" title="&laquo; voltar">&laquo; voltar</a>
		</div>

	</div>

@stop