@section('conteudo')
	
	<div class="pure-g" id="listaLinks">
		
		<div class="pure-u-2-3 pure-u-mobile-1">
			<div class="pad">
				
				@if(isset($textos[0]))
					<a href="{{$textos[0]->categoria->slug}}/{{$textos[0]->slug}}" title="{{$textos[0]->titulo}}" class="link-grande @if($textos[0]->capa) comimagem @endif">
						@if($textos[0]->capa)
							<div class="capa">
								<img src="assets/images/textos/capas/grande/{{$textos[0]->capa}}" alt="{{$textos[0]->titulo}}">
								@if($textos[0]->video_destaque)
									<div class="play"></div>
								@endif
							</div>
						@endif
						<div class="miolo">
							<h1 @if(!$textos[0]->capa && $textos[0]->video_destaque) class="complay" @endif><span>{{$textos[0]->titulo}}</span></h1>
							<h2>{{$textos[0]->olho}}</h2>
							<div class="barra-inferior">
								<div class="data">{{Tools::converteDataFrontend($textos[0]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[0]->id)}}</div>
							</div>
						</div>
					</a>
				@endif

				@if(isset($textos[1]))
					<a href="{{$textos[1]->categoria->slug}}/{{$textos[1]->slug}}" title="{{$textos[1]->titulo}}" class="link-grande @if($textos[1]->capa) comimagem @endif">
						@if($textos[1]->capa)
							<div class="capa">
								<img src="assets/images/textos/capas/grande/{{$textos[1]->capa}}" alt="{{$textos[1]->titulo}}">
								@if($textos[1]->video_destaque)
									<div class="play"></div>
								@endif
							</div>
						@endif
						<div class="miolo">
							<h1 @if(!$textos[1]->capa && $textos[1]->video_destaque) class="complay" @endif><span>{{$textos[1]->titulo}}</span></h1>
							<h2>{{$textos[1]->olho}}</h2>
							<div class="barra-inferior">
								<div class="data">{{Tools::converteDataFrontend($textos[1]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[1]->id)}}</div>
							</div>
						</div>
					</a>
				@endif

				@if(isset($textos[2]))
					<a href="{{$textos[2]->categoria->slug}}/{{$textos[2]->slug}}" title="{{$textos[2]->titulo}}" class="link-grande @if($textos[2]->capa) comimagem @endif">
						@if($textos[2]->capa)
							<div class="capa">
								<img src="assets/images/textos/capas/grande/{{$textos[2]->capa}}" alt="{{$textos[2]->titulo}}">
								@if($textos[2]->video_destaque)
									<div class="play"></div>
								@endif
							</div>
						@endif
						<div class="miolo">
							<h1 @if(!$textos[2]->capa && $textos[2]->video_destaque) class="complay" @endif><span>{{$textos[2]->titulo}}</span></h1>
							<h2>{{$textos[2]->olho}}</h2>
							<div class="barra-inferior">
								<div class="data">{{Tools::converteDataFrontend($textos[2]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[2]->id)}}</div>
							</div>
						</div>
					</a>
				@endif

				@if(isset($textos[3]))
					<a href="{{$textos[3]->categoria->slug}}/{{$textos[3]->slug}}" title="{{$textos[3]->titulo}}" class="link-grande @if($textos[3]->capa) comimagem @endif">
						@if($textos[3]->capa)
							<div class="capa">
								<img src="assets/images/textos/capas/grande/{{$textos[3]->capa}}" alt="{{$textos[3]->titulo}}">
								@if($textos[3]->video_destaque)
									<div class="play"></div>
								@endif
							</div>
						@endif
						<div class="miolo">
							<h1 @if(!$textos[3]->capa && $textos[3]->video_destaque) class="complay" @endif><span>{{$textos[3]->titulo}}</span></h1>
							<h2>{{$textos[3]->olho}}</h2>
							<div class="barra-inferior">
								<div class="data">{{Tools::converteDataFrontend($textos[3]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[3]->id)}}</div>
							</div>
						</div>
					</a>
				@endif

				@if(isset($textos[4]))
					<a href="{{$textos[4]->categoria->slug}}/{{$textos[4]->slug}}" title="{{$textos[4]->titulo}}" class="link-grande @if($textos[4]->capa) comimagem @endif">
						@if($textos[4]->capa)
							<div class="capa">
								<img src="assets/images/textos/capas/grande/{{$textos[4]->capa}}" alt="{{$textos[4]->titulo}}">
								@if($textos[4]->video_destaque)
									<div class="play"></div>
								@endif
							</div>
						@endif
						<div class="miolo">
							<h1 @if(!$textos[4]->capa && $textos[4]->video_destaque) class="complay" @endif><span>{{$textos[4]->titulo}}</span></h1>
							<h2>{{$textos[4]->olho}}</h2>
							<div class="barra-inferior">
								<div class="data">{{Tools::converteDataFrontend($textos[4]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[4]->id)}}</div>
							</div>
						</div>
					</a>
				@endif

			</div>
		</div>

		<div class="pure-u-1-3 pure-u-mobile-1">
			
			@if(isset($textos[5]))
				<a href="{{$textos[5]->categoria->slug}}/{{$textos[5]->slug}}" title="{{$textos[5]->titulo}}" class="link-pequeno pure-u-1 @if($textos[5]->capa) comimagem @endif">
					@if($textos[5]->capa)
						<div class="capa">
							<img src="assets/images/textos/capas/pequena/{{$textos[5]->capa}}" alt="{{$textos[5]->titulo}}">
							@if($textos[5]->video_destaque)
								<div class="play"></div>
							@endif
						</div>
					@endif
					<div class="miolo">
						<div class="barra-superior">
							<div class="data">{{Tools::converteDataFrontend($textos[5]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[5]->id)}}</div>
						</div>
						<h1>{{$textos[5]->titulo}}</h1>
						<h2>{{$textos[5]->olho}}</h2>
						<div class="barra-inferior">
							<div class="data">{{Tools::converteDataFrontend($textos[5]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[5]->id)}}</div>
						</div>
					</div>
				</a>
			@endif

			@if(isset($textos[6]))
				<a href="{{$textos[6]->categoria->slug}}/{{$textos[6]->slug}}" title="{{$textos[6]->titulo}}" class="link-pequeno pure-u-1 @if($textos[6]->capa) comimagem @endif">
					@if($textos[6]->capa)
						<div class="capa">
							<img src="assets/images/textos/capas/pequena/{{$textos[6]->capa}}" alt="{{$textos[6]->titulo}}">
							@if($textos[6]->video_destaque)
								<div class="play"></div>
							@endif
						</div>
					@endif
					<div class="miolo">
						<div class="barra-superior">
							<div class="data">{{Tools::converteDataFrontend($textos[6]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[6]->id)}}</div>
						</div>
						<h1>{{$textos[6]->titulo}}</h1>
						<h2>{{$textos[6]->olho}}</h2>
						<div class="barra-inferior">
							<div class="data">{{Tools::converteDataFrontend($textos[6]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[6]->id)}}</div>
						</div>
					</div>
				</a>
			@endif

			@if(isset($textos[7]))
				<a href="{{$textos[7]->categoria->slug}}/{{$textos[7]->slug}}" title="{{$textos[7]->titulo}}" class="link-pequeno pure-u-1 @if($textos[7]->capa) comimagem @endif">
					@if($textos[7]->capa)
						<div class="capa">
							<img src="assets/images/textos/capas/pequena/{{$textos[7]->capa}}" alt="{{$textos[7]->titulo}}">
							@if($textos[7]->video_destaque)
								<div class="play"></div>
							@endif
						</div>
					@endif
					<div class="miolo">
						<div class="barra-superior">
							<div class="data">{{Tools::converteDataFrontend($textos[7]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[7]->id)}}</div>
						</div>
						<h1>{{$textos[7]->titulo}}</h1>
						<h2>{{$textos[7]->olho}}</h2>
						<div class="barra-inferior">
							<div class="data">{{Tools::converteDataFrontend($textos[7]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[7]->id)}}</div>
						</div>
					</div>
				</a>
			@endif						

			@if(isset($textos[8]))
				<a href="{{$textos[8]->categoria->slug}}/{{$textos[8]->slug}}" title="{{$textos[8]->titulo}}" class="link-pequeno pure-u-1 @if($textos[8]->capa) comimagem @endif">
					@if($textos[8]->capa)
						<div class="capa">
							<img src="assets/images/textos/capas/pequena/{{$textos[8]->capa}}" alt="{{$textos[8]->titulo}}">
							@if($textos[8]->video_destaque)
								<div class="play"></div>
							@endif
						</div>
					@endif
					<div class="miolo">
						<div class="barra-superior">
							<div class="data">{{Tools::converteDataFrontend($textos[8]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[8]->id)}}</div>
						</div>
						<h1>{{$textos[8]->titulo}}</h1>
						<h2>{{$textos[8]->olho}}</h2>
						<div class="barra-inferior">
							<div class="data">{{Tools::converteDataFrontend($textos[8]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[8]->id)}}</div>
						</div>
					</div>
				</a>
			@endif

			@if(isset($textos[9]))
				<a href="{{$textos[9]->categoria->slug}}/{{$textos[9]->slug}}" title="{{$textos[9]->titulo}}" class="link-pequeno pure-u-1 @if($textos[9]->capa) comimagem @endif">
					@if($textos[9]->capa)
						<div class="capa">
							<img src="assets/images/textos/capas/pequena/{{$textos[9]->capa}}" alt="{{$textos[9]->titulo}}">
							@if($textos[9]->video_destaque)
								<div class="play"></div>
							@endif
						</div>
					@endif
					<div class="miolo">
						<div class="barra-superior">
							<div class="data">{{Tools::converteDataFrontend($textos[9]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[9]->id)}}</div>
						</div>
						<h1>{{$textos[9]->titulo}}</h1>
						<h2>{{$textos[9]->olho}}</h2>
						<div class="barra-inferior">
							<div class="data">{{Tools::converteDataFrontend($textos[9]->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($textos[9]->id)}}</div>
						</div>
					</div>
				</a>
			@endif

		</div>

		<div id="paginacao" class="pure-u-1">
			{{$textos->links()}}
		</div>

	</div>

@stop
