@section('conteudo')

	<div id="buscaAvancada">
		<form action="busca" method="get">
			<input type="text" name="termoBusca" placeholder="buscar..." @if(isset($termo)) value="{{$termo}}" @endif>
			<label><input type="checkbox" name="buscarNoticias" value="1" @if(in_array('buscarNoticias', $scopes)) checked @endif> NOTÍCIAS</label>
			<label><input type="checkbox" name="buscarEventos" value="1" @if(in_array('buscarEventos', $scopes)) checked @endif> EVENTOS</label>
			<label><input type="checkbox" name="buscarArtigos" value="1" @if(in_array('buscarArtigos', $scopes)) checked @endif> ARTIGOS</label>
			<label><input type="checkbox" name="buscarDicas" value="1" @if(in_array('buscarDicas', $scopes)) checked @endif> DICAS</label>
			<label><input type="checkbox" name="buscarPesquisas" value="1" @if(in_array('buscarPesquisas', $scopes)) checked @endif> PESQUISAS</label>
			<input type="submit" value="busca avançada &raquo;">
		</form>
	</div>

	<div id="numresultados">
		Foram encontrados <strong>{{$numero_resultados}} resultados</strong>
	</div>

	@if($resultados)
		<div id="listaResultados">
			@foreach($resultados as $resultado)
				<a href="{{$resultado->categoria->slug}}/{{$resultado->slug}}" title="{{$resultado->titulo}}" class="link-medio @if($resultado->capa) comimagem @endif">
					@if($resultado->capa)
						<div class="capa">
							<img src="assets/images/textos/capas/pequena/{{$resultado->capa}}" alt="{{$resultado->titulo}}">
							@if($resultado->video_destaque)
								<div class="play"></div>
							@endif
						</div>
					@endif
					<div class="miolo">
						<div class="labelCategoria @if(!$resultado->capa && $resultado->video_destaque) complay @endif">{{$resultado->categoria->titulo}}</div>
						<div class="barra-superior">
							<div class="data">{{Tools::converteDataFrontend($resultado->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($resultado->id)}}</div>
						</div>
						<h1>{{$resultado->titulo}}</h1>
						<h2>{{$resultado->olho}}</h2>
						<div class="barra-inferior">
							<div class="data">{{Tools::converteDataFrontend($resultado->data_publicacao)}}</div><div class="comentarios">{{Tools::numeroComentarios($resultado->id)}}</div>
						</div>
					</div>
				</a>
			@endforeach

			
			<div id="paginacao" class="pure-u-1">
				{{$resultados->appends($appendPagination)->links()}}
			</div>
		</div>
	@endif

@stop