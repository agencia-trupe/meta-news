@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Imagens do Texto - {{$texto->titulo}} <a href='{{ URL::route('painel.textosimagens.create', array('textos_id' => $texto->id)) }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Imagem</a>
    </h2>

    <a href="{{URL::route('painel.textos.index', array('categoria' => $texto->categoria->slug))}}" class="btn btn-default btn-sm">&larr; voltar</a>

    <table class='table table-striped table-bordered table-hover table-sortable' data-tabela='textos_imagens'>

        <thead>
            <tr>
                <th>Ordenar</th>
				<th>Imagem</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($texto->imagens as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm'>mover</a></td>
				<td><img src='assets/images/textosimagens/thumbs/{{ $registro->imagem }}' style='max-width:150px'></td>
                <td class="crud-actions">
                    {{ Form::open(array('route' => array('painel.textosimagens.destroy', $registro->id), 'method' => 'delete')) }}
						<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
					{{ Form::close() }}                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    
</div>

@stop