@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Textos em Destaque na Home @if(sizeof(\Textos::destaques()) < 2) <a href='{{ URL::route('painel.destaques.create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Destaque</a> @endif
    </h2>

    <table class='table table-striped table-bordered table-hover table-sortable' data-tabela='destaques'>

        <thead>
            <tr>
                <th>Título</th>
                <th>Data de Publicação</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td>{{ $registro->titulo }} @if($registro->destaque) <span class="label label-success">destaque na home</span> @endif </td>
                <td>{{ Tools::converteData($registro->data_publicacao) }}</td>
                <td class="crud-actions">
                	{{ Form::open(array('route' => array('painel.destaques.destroy', $registro->id), 'method' => 'delete')) }}
						<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
					{{ Form::close() }}
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    
</div>

@stop