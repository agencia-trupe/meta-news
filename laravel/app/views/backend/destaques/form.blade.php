@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Destaque
        </h2>  

		<form action="{{URL::route('painel.destaques.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputSelCategoria">Categoria</label>
					<select name="" class="form-control" id="inputSelCategoria" required>
						<option value="">Selecione</option>
						@foreach(\TextosCategorias::all() as $c)
							<option value="{{$c->slug}}">{{$c->titulo}}</option>
						@endforeach
					</select>
				</div>

				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<select name="textos_id" class="form-control" id="inputSelTextos" required>
						<option value="">Selecione uma categoria</option>
					</select>
				</div>
				
				<br>
				
				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.quemsomos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop