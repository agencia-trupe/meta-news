@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Categoria
        </h2>

		<form action="{{URL::route('painel.perguntasfrequentes.categorias.store')}}" method="post">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

		    	<div class="form-group">
		    		<label for="inputTitulo">Título</label>
					<input type="text" class="form-control" id="inputTitulo" name="titulo" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['titulo'] }}" @endif required>
		    	</div>

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.perguntasfrequentes.categorias.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>

@stop
