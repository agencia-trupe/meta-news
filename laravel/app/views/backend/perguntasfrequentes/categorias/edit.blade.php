@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Categoria
        </h2>

		{{ Form::open( array('route' => array('painel.perguntasfrequentes.categorias.update', $registro->id), 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

                <div class="form-group">
                    <label for="inputTitulo">Título</label>
                    <input type="text" class="form-control" id="inputTitulo" name="titulo" value="{{ $registro->titulo }}" required>
                </div>

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.perguntasfrequentes.categorias.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>

@stop
