@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Perguntas Frequentes
        <div class="btn-group pull-right">
            <a href="{{ URL::route('painel.perguntasfrequentes.categorias.index') }}" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-edit"></span> Editar Categorias</a>
            <a href='{{ URL::route('painel.perguntasfrequentes.create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Pergunta</a>
        </div>
    </h2>

    <table class='table table-striped table-bordered table-hover table-sortable' data-tabela='perguntas_frequentes'>

        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Categoria</th>
				<th>Pergunta</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm'>mover</a></td>
                <td>{{ $registro->categoria->titulo or null }}</td>
				<td>{{ $registro->pergunta }}</td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.perguntasfrequentes.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
						{{ Form::open(array('route' => array('painel.perguntasfrequentes.destroy', $registro->id), 'method' => 'delete')) }}
							<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
						{{ Form::close() }}
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>


</div>

@stop
