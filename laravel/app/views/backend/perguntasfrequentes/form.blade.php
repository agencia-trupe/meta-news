@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Pergunta
        </h2>

		<form action="{{URL::route('painel.perguntasfrequentes.store')}}" method="post">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

		    	<div class="form-group">
		    		<label for="categoria_id">Categoria</label>
		    		{{ Form::select('categoria_id', ['' => 'Selecione'] + $categorias, null, ['class' => 'form-control']) }}
		    	</div>

		    	<div class="form-group">
		    		<label for="inputTexti">Pergunta</label>
		    		<textarea name="pergunta" class="form-control" id="inputTexti" required>@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['pergunta'] }} @endif</textarea>
		    	</div>

				<div class="form-group">
					<label for="inputTexti">Resposta</label>
					<textarea name="resposta" class="form-control perguntasfrequentes" id="inputTexti" required>@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['resposta'] }} @endif</textarea>
				</div>


				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.perguntasfrequentes.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>

@stop
