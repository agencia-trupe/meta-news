<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="robots" content="index, nofollow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2013 Trupe Design" />
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <title>Meta News - Painel Administrativo</title>
	<meta name="description" content="">
    <meta name="keywords" content="" />

	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>

	<?=Assets::CSS(array(
		'vendor/reset-css/reset',
		'css/fontface/stylesheet',
		'vendor/jquery-ui/themes/base/jquery-ui',
		'vendor/bootstrap/dist/css/bootstrap.min',
		'vendor/bootstrap/dist/css/bootstrap-theme.min',
		'css/painel/painel'
	))?>

	@if(isset($css))
		<?=Assets::CSS(array($css))?>
	@endif

	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="assets/vendor/jquery/dist/jquery.min.js"><\/script>')</script>

	@if(App::environment()=='local')
		<?=Assets::JS(array('js/vendor/modernizr/modernizr', 'vendor/less.js/dist/less-1.6.2.min'))?>
	@else
		<?=Assets::JS(array('js/vendor/modernizr/modernizr'))?>
	@endif

</head>
	<body @if(Route::currentRouteName() == 'painel.login') class="body-painel-login" @else class="body-painel" @endif >

		@if(Route::currentRouteName() != 'painel.login')

			<nav class="navbar navbar-default">
				<div class="navbar-inner">
					<a href="{{URL::route('painel.home')}}" title="Página Inicial" class="navbar-brand">Meta News</a>

					<ul class="nav navbar-nav">

						<li @if(str_is('painel.home*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.home')}}" title="Início">Início</a>
						</li>

						<li class="dropdown @if(str_is('painel.banne*', Route::currentRouteName())) active @endif ">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Home <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{URL::route('painel.banners.index')}}" title="Notícias">Banners</a></li>
								<li><a href="{{URL::route('painel.bannerinferior.index')}}" title="Eventos">Banner Inferior</a></li>
							</ul>
						</li>

						<li @if(str_is('painel.quemsomos*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.quemsomos.index')}}" title="Quem Somos">Quem Somos</a>
						</li>

						<li @if(str_is('painel.equipe*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.equipe.index')}}" title="Equipe">Equipe</a>
						</li>

						<li @if(str_is('painel.imprensa*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.imprensa.index')}}" title="Imprensa">Imprensa</a>
						</li>

						<li @if(str_is('painel.perguntasfrequentes*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.perguntasfrequentes.index')}}" title="Perguntas Frequentes">Perguntas Frequentes</a>
						</li>

						<li @if(str_is('painel.contato*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.contato.index')}}" title="Contato">Contato</a>
						</li>

						<li @if(str_is('painel.enquete*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.enqueteperguntas.index')}}" title="Enquetes">Enquetes</a>
						</li>

						<li class="dropdown @if(str_is('painel.textos*', Route::currentRouteName()) || str_is('painel.destaques*', Route::currentRouteName()) || str_is('painel.compartilhamentos*', Route::currentRouteName())) active @endif ">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Textos <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{URL::route('painel.textos.index', array('categoria' => 'noticias'))}}" title="Notícias">Notícias</a></li>
								<li><a href="{{URL::route('painel.textos.index', array('categoria' => 'reportagens'))}}" title="Reportagens">Reportagens</a></li>
								<li><a href="{{URL::route('painel.textos.index', array('categoria' => 'entrevistas'))}}" title="Entrevistas">Entrevistas</a></li>
								<li><a href="{{URL::route('painel.textos.index', array('categoria' => 'eventos'))}}" title="Eventos">Eventos</a></li>
								<li><a href="{{URL::route('painel.textos.index', array('categoria' => 'artigos'))}}" title="Artigos">Artigos</a></li>
								<li><a href="{{URL::route('painel.textos.index', array('categoria' => 'dicas'))}}" title="Dicas">Dicas</a></li>
								<li><a href="{{URL::route('painel.textos.index', array('categoria' => 'pesquisas'))}}" title="Pesquisas">Pesquisas</a></li>
								<li role="presentation" class="divider"></li>
								<li><a href="{{URL::route('painel.destaques.index')}}" title="Destaques da Home">Destaques da Home</a></li>
								<li role="presentation" class="divider"></li>
								<li><a href="{{URL::route('painel.compartilhamentos.index')}}" title="Compartilhamentos">Compartilhamentos</a></li>
							</ul>
						</li>

						<li @if(str_is('painel.politica*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.politica.index')}}" title="Política de Privacidade">Política de Privacidade</a>
						</li>

						<li class="dropdown @if(str_is('painel.usuarios*', Route::currentRouteName())) active @endif ">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Sistema <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{URL::route('painel.cadastros.index')}}" title="Cadastros de Newsletter">Cadastros de Newsletter</a></li>
								<li><a href="{{URL::route('painel.usuarios.index')}}" title="Usuários do Painel">Usuários</a></li>
								<li><a href="{{URL::route('painel.off')}}" title="Logout">Logout</a></li>
							</ul>
						</li>

					</ul>
				</div>
			</nav>

		@endif

		<div class="main {{ 'main-'.str_replace('.', '-', Route::currentRouteName()) }}">
			@yield('conteudo')
		</div>

		<footer>
			<div class="container">
				<a href="http://www.trupe.net" target="_blank" title="Criação de Sites : Trupe Agência Criativa">© Criação de Sites : Trupe Agência Criativa</a>
			</div>
		</footer>

		@if(Route::currentRouteName() == 'painel.login')
			<?=Assets::JS(array(
				'vendor/jquery-ui/ui/minified/jquery-ui.min',
				'vendor/bootbox/bootbox',
				'vendor/ckeditor/ckeditor',
				'vendor/ckeditor/adapters/jquery',
				'vendor/bootstrap/dist/js/bootstrap.min',
				'js/painel/login'
			))?>
		@else
			<?=Assets::JS(array(
				'vendor/jquery-ui/ui/minified/jquery-ui.min',
				'vendor/bootbox/bootbox',
				'vendor/ckeditor/ckeditor',
				'vendor/ckeditor/adapters/jquery',
				'vendor/bootstrap/dist/js/bootstrap.min',
				'js/painel/painel'
			))?>
		@endif

	</body>
</html>
