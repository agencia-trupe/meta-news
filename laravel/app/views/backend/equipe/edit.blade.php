@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Membro da Equipe
        </h2>  

		{{ Form::open( array('route' => array('painel.equipe.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputNome">Nome</label>
					<input type="text" class="form-control" id="inputNome" name="nome" value="{{$registro->nome}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputCargo">Cargo</label>
					<input type="text" class="form-control" id="inputCargo" name="cargo" value="{{$registro->cargo}}" >
				</div>
				
				<div class="form-group">
					<label for="inputE-mail">E-mail</label>
					<input type="text" class="form-control" id="inputE-mail" name="email" value="{{$registro->email}}" >
				</div>
				
				<div class="form-group">
					<label for="inputTexti">Texto</label>
					<textarea name="texto" class="form-control cke" id="inputTexti" >{{$registro->texto }}</textarea>
				</div>
				

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.equipe.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop