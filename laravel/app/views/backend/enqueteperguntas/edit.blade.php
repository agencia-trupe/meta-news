@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Enquete
        </h2>  

		{{ Form::open( array('route' => array('painel.enqueteperguntas.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputPergunta">Pergunta</label>
					<textarea name="texto" class="form-control cke" id="inputPergunta" >{{$registro->texto }}</textarea>
				</div>

				<div class="form-group">
					<label for="inputData">Data</label>
					<input type="text" required name="data" id="inputData" class="form-control datepicker"  value="{{Tools::converteData($registro->data)}}">
				</div>

				<hr>

				<label>Respostas:</label><br>

				<div id="opcoes-control">
					<button type="button" class="btn btn-success btn-sm" title="Adicionar opção de resposta"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar opção de resposta</a>
				</div>

				<div id="listaOpcoes">

					@if(sizeof($registro->opcoes))
						@foreach($registro->opcoes as $opcao)
							<div class="row">
								<div class="col-lg-12">
									<div class="input-group">
										<input type="text" value="{{$opcao->texto}}" class="form-control opcao-enquete" name="enquete_opcao[]" placeholder="Opção de Resposta">
										<span class="input-group-btn">
											<button class="btn btn-default btn-danger" type="button"><span class="glyphicon glyphicon-remove-circle"></span></button>
										</span>
									</div>
								</div>
							</div>
						@endforeach
					@endif

				</div>

				<hr>				

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.enqueteperguntas.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop