@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Enquetes <a href='{{ URL::route('painel.enqueteperguntas.create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Enquete</a>
    </h2>

    <hr>
        <div class="btn-group">
            <a href="{{URL::route('painel.enqueteperguntas.index') }}" class="btn btn-sm btn-default @if(!$historico) btn-info @endif">Enquetes Novas</a>
            <a href="{{URL::route('painel.enqueteperguntas.index', array('historico' => 1)) }}" class="btn btn-sm btn-default @if($historico) btn-info @endif">Histórico</a>
        </div>
    <hr>

    <div class="alert alert-warning text-center">
        O <strong>resultado parcial</strong> da enquente ativa fica disponível após o primeiro voto.
        <br>
        Depois de receber votos a enquete não pode mais ser editada.
        <br>
        Enquetes que já possuem votos são armazenadas no Histórico quando desativadas.        
    </div>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
                @if(!$historico)
                    <th>Ativa</th>
                @endif
                <th>Pergunta</th>
                <th>Data</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                @if(!$historico)
                    <td>
                        @if($registro->ativa == '1')
                            <span class="label label-success">enquete ativa <span class="glyphicon glyphicon-ok-circle"></span></span>
                        @else
                            <button type="button" class="ativarenquente btn btn-default btn-sm" data-idenquete="{{$registro->id}}">tornar ativa</button>
                        @endif
                    </td>
                @endif
                <td>{{ Str::words(strip_tags($registro->texto), 15) }}</td>
                <td>{{Tools::converteData($registro->data)}}</td>
                <td class="@if(!$historico) crud-actions @else crud-actions-lg @endif" style="width:225px !important;">
                    @if(!$historico)
                        @if(sizeof($registro->numeroVotos) == 0)
                            <a href='{{ URL::route('painel.enqueteperguntas.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
                        @else
                            <a href='{{ URL::route('painel.enqueteperguntas.show', $registro->id ) }}' class='btn btn-info btn-sm'>ver resultados parciais</a>
                        @endif
                    @else
                        <a href='{{URL::route('painel.enqueteperguntas.show', $registro->id)}}' class='btn btn-sm btn-info'>ver resultados</a>
                    @endif
					{{ Form::open(array('route' => array('painel.enqueteperguntas.destroy', $registro->id), 'method' => 'delete')) }}
						<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
					{{ Form::close() }}                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    {{ $registros->links() }}
</div>

@stop