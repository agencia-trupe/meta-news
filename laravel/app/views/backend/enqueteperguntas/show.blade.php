@section('conteudo')

    <div class="container add">

      	<h2>
        	Resultados da Enquete
        </h2>  

		
		<div class="pad">

			<div class="form-group">
				<label for="inputPergunta">Pergunta</label>
				<div class="well">
					{{$registro->texto }}					
				</div>
			</div>

			<hr>

			<label>Respostas:</label><br>

			<div class="well">
				Total de votos: {{ EnquetesHelper::totalDeVotos($registro->id) }}
			</div>

			<div id="listaResultado" class="well">

				@if(sizeof($registro->opcoes))
					@foreach($registro->opcoes as $opcao)
						{{$opcao->texto}} - {{EnquetesHelper::porcentagemDeVotos($opcao->id)}} ({{ EnquetesHelper::votosNaOpcao($opcao->id) }})<br>
						<div class="progress">
							<div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="{{EnquetesHelper::porcentagemDeVotos($opcao->id)}}" aria-valuemin="0" aria-valuemax="100" style="width:{{EnquetesHelper::porcentagemDeVotos($opcao->id)}};">
						    	<span class="sr-only">{{EnquetesHelper::porcentagemDeVotos($opcao->id)}}</span>
						  	</div>
						</div>
					@endforeach
				@endif

			</div>

			<hr>				

			<a href="{{URL::previous()}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</div>
		
    </div>
    
@stop