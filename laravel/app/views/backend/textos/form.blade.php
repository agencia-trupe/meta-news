@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar {{$categoria->singular}}
        </h2>  

		<form action="{{URL::route('painel.textos.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

				<input type="hidden" name="textos_categorias_id" value="{{$categoria->id}}">
				
				<div class="form-group">
					<label for="inputTítulo">Título <span class="text-danger">*</span></label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['titulo'] }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputCapa">Imagem de Capa</label>
					<input type="file" class="form-control" id="inputCapa" name="capa">
				</div>
				
				<hr>

				<label>Mídia de Destaque</label>

				<div class="form-group" id="selecionaDestaque">
					<div>
						<label><input type="radio" name="selecionaTipoDestaque" value="imagem"> Imagem<br></label>
						<div class="removido">
							<input type="file" class="form-control" id="inputImagemdeDestaque" style="margin-top:3px;" name="imagem_destaque">
						</div>
					</div>
					<br><br>
					<div>
						<label><input type="radio" name="selecionaTipoDestaque" value="video"> Vídeo<br></label>
						<div class="removido">
							<input type="text" class="form-control" id="inputVídeodeDestaque" placeholder="colar o endereço do vídeo" style="width:100%; margin-top:3px;" name="video_destaque" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['video_destaque'] }}" @endif >
						</div>
					</div>
				</div>
				
				<hr>

				<div class="form-group">
					<label for="inputData de Publicação">Data de Publicação <span class="text-danger">*</span></label>
					<input type="text" class="form-control datepicker" id="inputData de Publicação" name="data_publicacao" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['data_publicacao'] }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputOlho">Olho</label>
					<textarea name="olho" class="form-control olho" id="inputOlho" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['olho'] }} @endif</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control cke" id="inputTexto" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['texto'] }} @endif</textarea>
				</div>
				

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.textos.index', array('categoria' => $categoria->slug))}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop