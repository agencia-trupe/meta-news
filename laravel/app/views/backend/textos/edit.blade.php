@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar {{$categoria->singular}}
        </h2>  

		{{ Form::open( array('route' => array('painel.textos.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<input type="hidden" name="textos_categorias_id" value="{{$categoria->id}}">
				
				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputCapa">Imagem de Capa</label>
					<br>
					<div class="well">
						@if($registro->capa)
							Capa Atual<br>
							<img src="assets/images/textos/capas/grande/{{$registro->capa}}">
							<br>
						@endif
						<br>
						@if($registro->capa)
							<label>Substituir Imagem</label>
						@endif
						<input type="file" class="form-control" id="inputCapa" name="capa">
						<br>
						@if($registro->capa)
							<label><input type="checkbox" name="remover_capa" value="1"> Não utilizar imagem de Capa</label>
						@endif
					</div>
				</div>

				<hr>

				<label>Mídia de Destaque</label>

				<div class="form-group" id="selecionaDestaque">
					<div>
						<label><input type="radio" name="selecionaTipoDestaque" value="imagem" @if($registro->imagem_destaque) checked @endif> Imagem<br></label>
						<div class="removido well">
							@if($registro->imagem_destaque)
								Imagem de Destaque Atual<br>
								<img src="assets/images/textos/imagem_destaque/redimensionada/{{$registro->imagem_destaque}}"><br>
							@endif
							<br>
							<label for="inputImagem de Destaque">{{ $registro->imagem_destaque ? 'Trocar' : 'Adicionar' }} Imagem de Destaque</label>
							<input type="file" class="form-control" id="inputImagemdeDestaque" name="imagem_destaque">
							<br>
							@if($registro->imagem_destaque)
								<label><input type="checkbox" name="removerDestaque" value="1"> Não utilizar imagem de Destaque</label>
							@endif
						</div>
					</div>
					<br><br>
					<div>
						<label><input type="radio" name="selecionaTipoDestaque" value="video" @if($registro->video_destaque) checked @endif> Vídeo<br></label>
						<div class="removido well">
							<input type="text" class="form-control" id="inputVídeodeDestaque" placeholder="colar o endereço do vídeo" style="width:100%; margin-top:3px;" name="video_destaque" value="{{$registro->video_destaque}}">
						</div>
					</div>
				</div>
				
				<hr>
				
				<div class="form-group">
					<label for="inputData de Publicação">Data de Publicação</label>
					<input type="text" class="form-control datepicker" id="inputData de Publicação" name="data_publicacao" value="{{ Tools::converteData($registro->data_publicacao) }}" required>
				</div>
				
				<div class="form-group">
					<label for="inputOlho">Olho</label> 
					<textarea name="olho" class="form-control olho" id="inputOlho" >{{$registro->olho }}</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control cke" id="inputTexto" >{{$registro->texto }}</textarea>
				</div>
				

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.textos.index', array('categoria' => $registro->categoria->slug))}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop