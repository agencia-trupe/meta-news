@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        {{$categoria->titulo}} <a href='{{ URL::route('painel.textos.create', array('categoria' => $categoria->slug)) }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar {{$categoria->singular}}</a>
    </h2>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
                <th>Título</th>
				<th>Data de Publicação</th>
                <th>Imagem</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td>{{ $registro->titulo }} @if($registro->destaque) <span class="label label-success">destaque na home</span> @endif </td>
				<td>{{ Tools::converteData($registro->data_publicacao) }}</td>
                <td><a href="{{URL::route('painel.textosimagens.index', array('id_texto' => $registro->id))}}" class="btn btn-sm btn-info" title="Gerenciar Imagens">gerenciar imagens</a></td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.textos.edit', array('id' => $registro->id, 'categoria' => $categoria->slug) ) }}' class='btn btn-primary btn-sm'>editar</a>
					{{ Form::open(array('route' => array('painel.textos.destroy', $registro->id), 'method' => 'delete')) }}
						<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
					{{ Form::close() }}                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    {{ $registros->appends(array('categoria' => $categoria->slug))->links() }}
</div>

@stop