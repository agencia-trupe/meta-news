@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Contato 
    </h2>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
                <th>Texto</th>
				<th>Telefone</th>
				<th>E-mail</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td>{{ Str::words(strip_tags($registro->texto), 15) }}</td>
				<td>{{ $registro->prefixo_telefone .' '.$registro->telefone }}</td>
				<td>{{ $registro->email }}</td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.contato.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    
</div>

@stop