@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Informações de Contato
        </h2>  

		{{ Form::open( array('route' => array('painel.contato.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control" id="inputTexto" >{{$registro->texto }}</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputTelefone">Prefixo / Telefone</label><br>
					<input type="text" class="form-control" style="width:15%; display:inline-block" id="inputPrefixoTelefone" name="prefixo_telefone" value="{{$registro->prefixo_telefone}}" >
					<input type="text" class="form-control" style="width:80%; display:inline-block" id="inputTelefone" name="telefone" value="{{$registro->telefone}}" >
				</div>
				
				<div class="form-group">
					<label for="inputE-mail">E-mail</label>
					<input type="text" class="form-control cke" id="inputE-mail" name="email" value="{{$registro->email}}" >
				</div>
				

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.contato.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop