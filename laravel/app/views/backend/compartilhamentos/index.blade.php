@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Compartilhamentos 
    </h2>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
                <th>Quem Enviou</th>
                <th>Destinatários</th>
                <th>Mensagem</th>
                <th>Texto Enviado</th>        
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td>
                    Nome: {{{ $registro->nome }}}<br>
                    E-mail: {{{ $registro->email }}}
                </td>
                <td>
                    {{ str_replace(', ', '<br>', $registro->destinatarios) }}
                </td>
                <td>{{{ $registro->mensagem }}}</td>
                <td><a href="{{ $registro->url }}" target="_blank">{{ \Textos::find($registro->id_texto)->titulo }}</a></td>
				<td class="crud-actions">
                    {{ Form::open(array('route' => array('painel.compartilhamentos.destroy', $registro->id), 'method' => 'delete')) }}
                        <button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
                    {{ Form::close() }}   
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    {{ $registros->links() }}

    
</div>

@stop