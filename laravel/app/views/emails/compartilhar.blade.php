<!DOCTYPE html>
<html>
<head>
    <title>Mensagem de contato via formulário do site</title>
    <meta charset="utf-8">
</head>
<body>
	@if(@mensagem)
		<p>
			{{{ $nome }}} lhe enviou este artigo do site Meta News e escreveu a seguinte mensagem: "{{{ $mensagem }}}"
		</p>		
	@else
		<p>
			{{{ $nome }}} lhe enviou este artigo do site Meta News:
		</p>
	@endif

	<hr>

	<h1>{{ $titulo }}</h1>

	<div class="texto">{{ $texto }}</div>

	<hr>

	<p>
		Para acessar o conteúdo completo acesse:<br>
		<a href="{{ $url }}" target="_blank">Meta News : {{ $titulo }}</a>
	</p>
</body>
</html>
