<?php

use \Textos, \TextosCategorias, \Banner, \EnquetePergunta, \Cadastro;

class HomeController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.home.index')->with('banners', Banner::ordenado())
																	->with('enquete', EnquetePergunta::ativa())
																	->with('destaques', Textos::destaques())
																	->with('textos', Textos::naodestaques());
	}

	public function cadastro()
	{
		$nome = Input::get('newsletterNome');
		$email = Input::get('newsletterEmail');

		if(sizeof(Cadastro::where('email', '=', $email)->get())){
			Session::flash('alerta', 'O e-mail informado já está cadastrado');
			return Redirect::back();
		}else{
			
			$cadastro = new Cadastro;
			$cadastro->nome = $nome;
			$cadastro->email = $email;
			$cadastro->save();

			Session::flash('alerta', 'E-mail cadastrado com sucesso!');
			return Redirect::back();
		}
	}
}
