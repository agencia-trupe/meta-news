<?php

use \QuemSomos;

class QuemSomosController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->with('pagetitle', 'Quem Somos');
		$this->layout->content = View::make('frontend.quemsomos.index')->with('texto', QuemSomos::first());
	}

}
