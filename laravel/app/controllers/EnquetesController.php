<?php

use \EnquetePergunta, \EnqueteVotos, \EnqueteOpcoes;

class EnquetesController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.enquetes.index')->with('ativa', EnquetePergunta::ativa())
																	  ->with('historico', EnquetePergunta::historico()->paginate(10));
	}

	public function resultados($dia = '', $mes = '', $ano = '', $id = '')
	{
		$detalhe = EnquetePergunta::where(DB::raw('YEAR(data)'), '=', $ano)
								  ->where(DB::raw('MONTH(data)'), '=', $mes)
								  ->where(DB::raw('DAY(data)'), '=', $dia)
								  ->where('id', '=', $id)
								  ->first();

		if(!$detalhe)
			App::abort('404');

		$this->layout->with('css', 'css/enquetes');
		$this->layout->content = View::make('frontend.enquetes.resultados')->with('detalhe', $detalhe);
	}

	public function votar()
	{
		$email = Input::get('emailVoto');
		$voto = Input::get('opcaoVoto');

		if(!$email){
			Session::flash('alerta', 'O e-mail é obrigatório para computarmos o voto!');
			return Redirect::back();
		}

		$opcao = EnqueteOpcoes::where('id', '=', $voto)->first();
		
		if(is_null($opcao)){
			Session::flash('alerta', 'Erro ao inserir voto.');
			return Redirect::back();
		}

		if(sizeof(EnqueteVotos::where('email', '=', $email)->where('enquete_perguntas_id', '=', $opcao->enquete_pergunta_id)->get())){
			Session::flash('alerta', 'Você já votou nesta enquete!');
			Session::put('enqueteVotada', $opcao->enquete_pergunta_id);
			return Redirect::back();
		}

		$voto = new EnqueteVotos;
		$voto->enquete_opcoes_id = $opcao->id;
		$voto->enquete_perguntas_id = $opcao->enquete_pergunta_id;
		$voto->email = $email;
		$voto->save();
		Session::flash('alerta', 'Seu voto foi computado com sucesso!');
		Session::put('enqueteVotada', $voto->enquete_perguntas_id);
		return Redirect::back();
	}

}
