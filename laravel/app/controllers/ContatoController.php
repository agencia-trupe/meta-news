<?php

use \Contato, \Input, \Mail, \Session, \Redirect;

class ContatoController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->with('pagetitle', 'Contato');
		$this->layout->content = View::make('frontend.contato.index')->with('contato', Contato::first());
	}

	public function enviar()
	{
		$data['assunto'] = Input::get('contatoAssunto');
		$data['nome'] = Input::get('contatoNome');
		$data['email'] = Input::get('contatoEmail');
		$data['telefone'] = Input::get('contatoTelefone');
		$data['mensagem'] = Input::get('contatoMensagem');
		$data['ip_envio'] = Tools::ip();

		if($data['nome'] && $data['email'] && $data['mensagem']){
			Mail::send('emails.contato', $data, function($message) use ($data)
			{
			    $message->to('contato@metanews.com.br', 'Meta News')
			    		->subject($data['assunto'])
			    		->replyTo($data['email'], $data['nome']);
			});
			Session::flash('alerta', 'Mensagem enviada com sucesso!<br>Retornaremos assim que possível.');
		}else{
			Session::flash('alerta', 'Os campos de Assunto, Nome, Email e Mensagem são obrigatórios.');
			Session::flash('formContato.assunto', $data['assunto']);
			Session::flash('formContato.nome', $data['nome']);
			Session::flash('formContato.email', $data['email']);
			Session::flash('formContato.telefone', $data['telefone']);
			Session::flash('formContato.mensagem', $data['mensagem']);
		}
		return Redirect::route('contato');
	}

}
