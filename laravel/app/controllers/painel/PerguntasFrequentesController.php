<?php

namespace Painel;

use \View, \Input, \Session, \Redirect, \PerguntaFrequente, \PerguntaFrequenteCategoria;

class PerguntasFrequentesController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	public function index()
	{
		$registros = PerguntaFrequente::with('categoria')->ordenados()->get();

		$this->layout->content = View::make('backend.perguntasfrequentes.index')->with(compact('registros'));
	}

	public function create()
	{
		$categorias = PerguntaFrequenteCategoria::ordenados()->lists('titulo', 'id');
		$this->layout->content = View::make('backend.perguntasfrequentes.form', compact('categorias'));
	}

	public function store()
	{
		$input = Input::all();

		try {

			PerguntaFrequente::create($input);

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Pergunta adicionada com sucesso.');
			return Redirect::route('painel.perguntasfrequentes.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao adicionar pergunta!'));

		}
	}

	public function edit($id)
	{
		$registro = PerguntaFrequente::findOrFail($id);
		$categorias = PerguntaFrequenteCategoria::ordenados()->lists('titulo', 'id');

		$this->layout->content = View::make('backend.perguntasfrequentes.edit')->with(compact('registro', 'categorias'));
	}

	public function update($id)
	{
		$object = PerguntaFrequente::find($id);
		$input  = Input::all();

		try {

			$object->update($input);

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Pergunta alterada com sucesso.');
			return Redirect::route('painel.perguntasfrequentes.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao alterar pergunta!'));

		}
	}

	public function destroy($id)
	{
		PerguntaFrequente::find($id)->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Pergunta removida com sucesso.');
		return Redirect::route('painel.perguntasfrequentes.index');
	}
}
