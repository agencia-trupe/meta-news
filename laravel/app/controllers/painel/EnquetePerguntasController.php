<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \EnquetePergunta, \EnqueteOpcoes, \EnquetesHelper;

class EnquetePerguntasController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if(Input::has('historico')){
			$enquetes = EnquetePergunta::historico()->paginate(25);
			$historico = true;
		}else{
			$enquetes = EnquetePergunta::semvotos()->paginate(25);
			$historico = false;
		}

		$this->layout->content = View::make('backend.enqueteperguntas.index')->with('registros', $enquetes)
																			 ->with('historico', $historico);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.enqueteperguntas.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new EnquetePergunta;

		$object->texto = Input::get('texto');
		$object->data = Tools::converteData(Input::get('data'));

		try {
	
			$object->save();

			if(Input::has('enquete_opcao')){
				$opcoes = Input::get('enquete_opcao');

				foreach ($opcoes as $key => $value) {
					if ($value) {
						$opcao = new EnqueteOpcoes;
						$opcao->enquete_pergunta_id = $object->id;
						$opcao->texto = $value;
						$opcao->ordem = $key;
						$opcao->save();						
					}
				}
			}

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Enquete criada com sucesso.');
			return Redirect::route('painel.enqueteperguntas.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Enquete!'));

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$this->layout->content = View::make('backend.enqueteperguntas.show')->with('registro', EnquetePergunta::find($id));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.enqueteperguntas.edit')->with('registro', EnquetePergunta::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = EnquetePergunta::find($id);

		$object->texto = Input::get('texto');
		$object->data = Tools::converteData(Input::get('data'));

		try {

			$object->save();

			if(Input::has('enquete_opcao')){
				$opcoes = Input::get('enquete_opcao');

				EnqueteOpcoes::where('enquete_pergunta_id', '=', $object->id)->delete();

				foreach ($opcoes as $key => $value) {
					if ($value) {
						$opcao = new EnqueteOpcoes;
						$opcao->enquete_pergunta_id = $object->id;
						$opcao->texto = $value;
						$opcao->ordem = $key;
						$opcao->save();						
					}
				}
			}

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Enquete alterada com sucesso.');
			return Redirect::route('painel.enqueteperguntas.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Enquete!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = EnquetePergunta::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Enquete removida com sucesso.');

		return Redirect::route('painel.enqueteperguntas.index');
	}

}