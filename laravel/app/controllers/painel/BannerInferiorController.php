<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, BannerInferior;

class BannerInferiorController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	public $limiteInsercao = '1';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.bannerinferior.index')->with('registros', BannerInferior::all())
																		   ->with('limiteInsercao', $this->limiteInsercao);		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.bannerinferior.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new BannerInferior;

		$imagem = Thumb::make('imagem', 980, 152, 'bannerinferior/');
		if($imagem) $object->imagem = $imagem;

		$object->link = Input::get('link');


		if($this->limiteInsercao && sizeof( BannerInferior::all() ) >= $this->limiteInsercao)
			return Redirect::back()->withErrors(array('Número máximo de Registros atingido!'));

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Banner criado com sucesso.');
			return Redirect::route('painel.bannerinferior.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Banner!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.bannerinferior.edit')->with('registro', BannerInferior::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = BannerInferior::find($id);

		$imagem = Thumb::make('imagem', 980, 152, 'bannerinferior/');
		if($imagem) $object->imagem = $imagem;

		$object->link = Input::get('link');


		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Banner alterado com sucesso.');
			return Redirect::route('painel.bannerinferior.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Banner!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = BannerInferior::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Banner removido com sucesso.');

		return Redirect::route('painel.bannerinferior.index');
	}

}