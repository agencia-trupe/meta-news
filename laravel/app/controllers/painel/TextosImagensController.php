<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \TextosImagem, \Textos;

class TextosImagensController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$id_texto = Input::get('id_texto');

		$this->layout->content = View::make('backend.textosimagens.index')->with('texto', Textos::find($id_texto));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$id_texto = Input::get('textos_id');

		$this->layout->content = View::make('backend.textosimagens.form')->with('texto', Textos::find($id_texto));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new TextosImagem;

		$t = date('YmdHis');
		$imagem = Thumb::make('imagem', 800, null, 'textosimagens/', $t);
		if($imagem) $object->imagem = $imagem;
		Thumb::make('imagem', 110, 110, 'textosimagens/thumbs/', $t);

		$object->textos_id = Input::get('textos_id');

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Imagem inserida com sucesso.');
			return Redirect::route('painel.textosimagens.index', array('id_texto' => $object->textos_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Imagem!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.textosimagens.edit')->with('registro', TextosImagem::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = TextosImagem::find($id);

		$t = date('YmdHis');
		$imagem = Thumb::make('imagem', 800, null, 'textosimagens/', $t);
		if($imagem) $object->imagem = $imagem;
		Thumb::make('imagem', 110, 110, 'textosimagens/thumbs/', $t);

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Imagem alterada com sucesso.');
			return Redirect::route('painel.textosimagens.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Imagem!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = TextosImagem::find($id);
		$id_parent = $object->textos_id;
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Imagem removida com sucesso.');

		return Redirect::route('painel.textosimagens.index', array('id_texto' => $id_parent));
	}

}