<?php

namespace Painel;

use \View, \Input, \Session, \Redirect, \PerguntaFrequenteCategoria;

class PerguntasFrequentesCategoriasController extends BaseAdminController {

    protected $layout = 'backend.templates.index';

    protected $limiteInsercao = false;

    public function index()
    {
        $registros = PerguntaFrequenteCategoria::ordenados()->get();

        $this->layout->content = View::make('backend.perguntasfrequentes.categorias.index')->with(compact('registros'));
    }

    public function create()
    {
        $this->layout->content = View::make('backend.perguntasfrequentes.categorias.form');
    }

    public function store()
    {
        $input = Input::all();

        try {

            PerguntaFrequenteCategoria::create($input);

            Session::flash('sucesso', true);
            Session::flash('mensagem', 'Categoria adicionada com sucesso.');
            return Redirect::route('painel.perguntasfrequentes.categorias.index');

        } catch (\Exception $e) {

            Session::flash('formulario', Input::all());
            return Redirect::back()->withErrors(array('Erro ao adicionar categoria!'));

        }
    }

    public function edit($id)
    {
        $registro = PerguntaFrequenteCategoria::findOrFail($id);

        $this->layout->content = View::make('backend.perguntasfrequentes.categorias.edit')->with(compact('registro'));
    }

    public function update($id)
    {
        $object = PerguntaFrequenteCategoria::find($id);
        $input  = Input::all();

        try {

            $object->update($input);

            Session::flash('sucesso', true);
            Session::flash('mensagem', 'Categoria alterada com sucesso.');
            return Redirect::route('painel.perguntasfrequentes.categorias.index');

        } catch (\Exception $e) {

            Session::flash('formulario', Input::all());
            return Redirect::back()->withErrors(array('Erro ao alterar categoria!'));

        }
    }

    public function destroy($id)
    {
        PerguntaFrequenteCategoria::find($id)->delete();

        Session::flash('sucesso', true);
        Session::flash('mensagem', 'Categoria removida com sucesso.');
        return Redirect::route('painel.perguntasfrequentes.categorias.index');
    }
}
