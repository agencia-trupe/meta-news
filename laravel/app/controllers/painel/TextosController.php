<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \Textos, \TextosCategorias;

class TextosController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$slug_categoria = Input::get('categoria');

		if(!$slug_categoria)
			return Redirect::route('painel.home');

		$categoria = TextosCategorias::where('slug', '=', $slug_categoria)->first();

		if(!$categoria)
			return Redirect::route('painel.home');

		$this->layout->content = View::make('backend.textos.index')->with('registros', $categoria->textos()->paginate(25))
																   ->with('categoria', $categoria);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$slug_categoria = Input::get('categoria');

		if(!$slug_categoria)
			return Redirect::route('painel.home');

		$categoria = TextosCategorias::where('slug', '=', $slug_categoria)->first();

		if(!$categoria)
			return Redirect::route('painel.home');

		$this->layout->content = View::make('backend.textos.form')->with('categoria', $categoria);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Textos;

		$object->textos_categorias_id = Input::get('textos_categorias_id');
		$object->titulo = strip_tags(Input::get('titulo'));

		if(Input::hasFile('capa')){
			$t = date('YmdHis');
			$capa = Thumb::make('capa', 470, 300, 'textos/capas/destaque_home/', $t);
			if($capa){
				$object->capa = $capa;
				Thumb::make('capa', 310, 180, 'textos/capas/grande/', $t);
				Thumb::make('capa', 110, 110, 'textos/capas/pequena/', $t);
				Thumb::make('capa', null, null, 'textos/capas/original/', $t);
			}
		}

		if(Input::has('selecionaTipoDestaque')){
			if(Input::get('selecionaTipoDestaque') == 'imagem'){
				$t = date('YmdHis');
				$imagem_destaque = Thumb::make('imagem_destaque', 610, null, 'textos/imagem_destaque/redimensionada/', $t);
				Thumb::make('imagem_destaque', null, null, 'textos/imagem_destaque/original/', $t);
				$object->imagem_destaque = $imagem_destaque;
				$object->video_destaque = '';
			}elseif(Input::get('selecionaTipoDestaque') == 'video'){
				$object->imagem_destaque = '';
				$object->video_destaque = Input::get('video_destaque');
			}
		}

		$object->data_publicacao = Tools::converteData(Input::get('data_publicacao'));
		$object->olho = Input::get('olho');
		$object->texto = Input::get('texto');
		$object->slug = Str::slug(Tools::slugData(Input::get('data_publicacao')).' '.Input::get('titulo'));

		$check_slug = sizeof(Textos::where('slug', '=', $object->slug)->get());
		if($check_slug > 0){
			$append = (int)$check_slug + 1;
			$object->slug = Str::slug(Tools::slugData(Input::get('data_publicacao')).' '.Input::get('titulo').' '.$append);
		}

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Texto criado com sucesso.');
			return Redirect::route('painel.textos.index', array('categoria' => TextosCategorias::find($object->textos_categorias_id)->slug));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Texto!'));

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$slug_categoria = Input::get('categoria');

		if(!$slug_categoria)
			return Redirect::route('painel.home');

		$categoria = TextosCategorias::where('slug', '=', $slug_categoria)->first();

		if(!$categoria)
			return Redirect::route('painel.home');

		$this->layout->content = View::make('backend.textos.edit')->with('registro', Textos::find($id))
																  ->with('categoria', $categoria);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Textos::find($id);

		$object->textos_categorias_id = Input::get('textos_categorias_id');
		$object->titulo = strip_tags(Input::get('titulo'));

		if(Input::has('remover_capa') && Input::get('remover_capa') == '1'){
			$object->capa = '';
		}else{
			$t = date('YmdHis');
			$capa = Thumb::make('capa', 470, 300, 'textos/capas/destaque_home/', $t);
			if($capa){
				$object->capa = $capa;
				Thumb::make('capa', 310, 180, 'textos/capas/grande/', $t);
				Thumb::make('capa', 110, 110, 'textos/capas/pequena/', $t);
				Thumb::make('capa', null, null, 'textos/capas/original/', $t);
			}
		}

		if(Input::has('selecionaTipoDestaque')){
			if(Input::get('selecionaTipoDestaque') == 'imagem'){
				$t = date('YmdHis');
				$imagem_destaque = Thumb::make('imagem_destaque', 610, null, 'textos/imagem_destaque/redimensionada/', $t);
				Thumb::make('imagem_destaque', null, null, 'textos/imagem_destaque/original/', $t);
				$object->imagem_destaque = $imagem_destaque;
				$object->video_destaque = '';
			}elseif(Input::get('selecionaTipoDestaque') == 'video'){
				$object->imagem_destaque = '';
				$object->video_destaque = Input::get('video_destaque');
			}
		}

		$object->data_publicacao = Tools::converteData(Input::get('data_publicacao'));
		$object->olho = Input::get('olho');
		$object->texto = Input::get('texto');
		$object->slug = Str::slug(Tools::slugData(Input::get('data_publicacao')).' '.Input::get('titulo'));

		$check_slug = sizeof(Textos::where('slug', '=', $object->slug)->where('id', '!=', $id)->get());
		if($check_slug > 0){
			$append = (int)$check_slug + 1;
			$object->slug = Str::slug(Tools::slugData(Input::get('data_publicacao')).' '.Input::get('titulo').' '.$append);
		}

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Texto alterado com sucesso.');
			return Redirect::route('painel.textos.index', array('categoria' => TextosCategorias::find($object->textos_categorias_id)->slug));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Texto!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Textos::find($id);
		$categoria = TextosCategorias::find($object->textos_categorias_id)->slug;
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Texto removido com sucesso.');

		return Redirect::route('painel.textos.index', array('categoria' => $categoria));
	}

}