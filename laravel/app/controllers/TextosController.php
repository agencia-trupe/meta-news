<?php

use \Textos;

class TextosController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function noticias()
	{
		$this->layout->with('htmlTitle', 'Notícias');
		$this->layout->with('css', 'css/textos');
		$this->layout->content = View::make('frontend.textos.index')->with('textos', Textos::noticias()->paginate(10));
	}

	public function reportagens()
	{
		$this->layout->with('htmlTitle', 'Reportagens');
		$this->layout->with('css', 'css/textos');
		$this->layout->content = View::make('frontend.textos.index')->with('textos', Textos::reportagens()->paginate(10));
	}

	public function entrevistas()
	{
		$this->layout->with('htmlTitle', 'Entrevistas');
		$this->layout->with('css', 'css/textos');
		$this->layout->content = View::make('frontend.textos.index')->with('textos', Textos::entrevistas()->paginate(10));
	}

	public function eventos()
	{
		$this->layout->with('htmlTitle', 'Eventos');
		$this->layout->with('css', 'css/textos');
		$this->layout->content = View::make('frontend.textos.index')->with('textos', Textos::eventos()->paginate(10));
	}

	public function artigos()
	{
		$this->layout->with('htmlTitle', 'Artigos');
		$this->layout->with('css', 'css/textos');
		$this->layout->content = View::make('frontend.textos.index')->with('textos', Textos::artigos()->paginate(10));
	}

	public function dicas()
	{
		$this->layout->with('htmlTitle', 'Dicas');
		$this->layout->with('css', 'css/textos');
		$this->layout->content = View::make('frontend.textos.index')->with('textos', Textos::dicas()->paginate(10));
	}

	public function pesquisas()
	{
		$this->layout->with('htmlTitle', 'Pesquisas');
		$this->layout->with('css', 'css/textos');
		$this->layout->content = View::make('frontend.textos.index')->with('textos', Textos::pesquisas()->paginate(10));
	}

	public function detalhes($slug = ''){
		if(!$slug) App::abort('404');

		$texto = Textos::where('slug', '=', $slug)->first();
		if(!$texto) App::abort('404');

		$this->layout->with('htmlTitle', $texto->categoria->titulo.' - '.$texto->titulo);
		$this->layout->with('css', 'css/textos');
		$this->layout->content = View::make('frontend.textos.detalhes')->with('texto', $texto)
																		->with('relacionados', Textos::relacionados($texto));
	}

	public function busca()
	{
		if(!Input::has('termoBusca'))
			return Redirect::to('home');

		$termo = Input::get('termoBusca');
		$appendPagination = array('termoBusca' => $termo);

		$scopes = array();

		if(Input::has('buscarNoticias') || Input::has('buscarEventos') || Input::has('buscarArtigos') || Input::has('buscarDicas') || Input::has('buscarPesquisas')){
			$models = Textos::where( function($query){
				$or = false;
				if(Input::has('buscarNoticias')){
					if($or)
						$query->orWhere('textos_categorias_id', '=', '1');
					else
						$query->where('textos_categorias_id', '=', '1');
					$or = true;
				}
				if(Input::has('buscarEventos')){
					if($or)
						$query->orWhere('textos_categorias_id', '=', '2');
					else
						$query->where('textos_categorias_id', '=', '2');
					$or = true;
				}
				if(Input::has('buscarArtigos')){
					if($or)
						$query->orWhere('textos_categorias_id', '=', '3');
					else
						$query->where('textos_categorias_id', '=', '3');
					$or = true;
				}
				if(Input::has('buscarDicas')){
					if($or)
						$query->orWhere('textos_categorias_id', '=', '4');
					else
						$query->where('textos_categorias_id', '=', '4');
					$or = true;
				}
				if(Input::has('buscarPesquisas')){
					if($or)
						$query->orWhere('textos_categorias_id', '=', '5');
					else
						$query->where('textos_categorias_id', '=', '5');
					$or = true;
				}
			});
			if(Input::has('buscarNoticias')){ array_push($scopes, 'buscarNoticias'); $appendPagination['buscarNoticias'] = '1'; }
			if(Input::has('buscarEventos')){ array_push($scopes, 'buscarEventos'); $appendPagination['buscarEventos'] = '1'; }
			if(Input::has('buscarArtigos')){ array_push($scopes, 'buscarArtigos'); $appendPagination['buscarArtigos'] = '1'; }
			if(Input::has('buscarDicas')){ array_push($scopes, 'buscarDicas'); $appendPagination['buscarDicas'] = '1'; }
			if(Input::has('buscarPesquisas')){ array_push($scopes, 'buscarPesquisas'); $appendPagination['buscarPesquisas'] = '1'; }
			$busca = $models->search($termo);
			$numero_resultados = sizeof($busca->get());
			$resultado = $busca->paginate(12);
		}else{
			$scopes = array('buscarNoticias', 'buscarEventos', 'buscarArtigos', 'buscarDicas', 'buscarPesquisas');
			$resultado = Textos::search($termo)->paginate(12);
			$numero_resultados = sizeof(Textos::search($termo)->get());
		}



		$this->layout->with('css', 'css/textos');
		$this->layout->with('pagetitle', "<small>RESULTADOS DA BUSCA ></small> <span class='busca'>".$termo."</span>");

		$this->layout->content = View::make('frontend.textos.busca')->with('resultados', $resultado)
																	->with('termo', $termo)
																	->with('numero_resultados', $numero_resultados)
																	->with('scopes', $scopes)
																	->with('appendPagination', $appendPagination);
	}

	public function compartilhar()
	{
		$data['nome'] = Input::get('nome');
		$data['email'] = Input::get('email');
		$data['destinatarios'] = Input::get('destino');
		$data['mensagem'] = Input::get('mensagem');
		$data['url'] = Input::get('url');
		$data['id_texto'] = Input::get('id');

		$texto = \Textos::find($data['id_texto']);

		if($data['nome'] && $data['email'] && $data['destinatarios'] && !is_null($texto)){

			$data['titulo'] = $texto->titulo;
			$data['texto'] = $texto->texto;

			Mail::send('emails.compartilhar', $data, function($message) use ($data)
			{
				if(str_is('*,*', $data['destinatarios'])){
					// Array de destinatários
					$dest_array = explode(',', $data['destinatarios']);
					foreach ($dest_array as $key => $value) {
						$message->to($value);
					}
				}else{
					// Destinatário único
					$message->to($data['destinatarios']);
				}

			    $message->subject($data['nome'].' lhe enviou este artigo do site Meta News')
			    		->replyTo($data['email'], $data['nome']);

			    $comp = new \Compartilhamentos;
			    $comp->nome = $data['nome'];
				$comp->email = $data['email'];
				$comp->destinatarios = $data['destinatarios'];
				$comp->mensagem = $data['mensagem'];
				$comp->url = $data['url'];
				$comp->id_texto = $data['id_texto'];
				$comp->save();
			});

			if(Config::get('mail.pretend')) Log::info(View::make('emails.compartilhar',$data)->render());
		}
	}
}
