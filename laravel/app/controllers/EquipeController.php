<?php

use \Equipe;

class EquipeController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->with('pagetitle', 'Equipe');
		$this->layout->content = View::make('frontend.equipe.index')->with('equipe', Equipe::ordenado());
	}

}
