<?php

use \Imprensa;

class ImprensaController extends \BaseController{

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->with('pagetitle', 'Imprensa');
		$this->layout->content = View::make('frontend.imprensa.index')->with('texto', Imprensa::first());
	}

}