<?php

use \PerguntaFrequente, \PerguntaFrequenteCategoria;

class PerguntasFrequentesController extends BaseController {

    protected $layout = 'frontend.templates.index';

    public function index()
    {
        $registros = PerguntaFrequenteCategoria::with('perguntas')->ordenados()->get();
        $semCategoria = PerguntaFrequente::has('categoria', '<', 1)->ordenados()->get();

        $this->layout->with('pagetitle', 'Perguntas Frequentes');
        $this->layout->content = View::make('frontend.perguntasfrequentes.index')->with(compact('registros', 'semCategoria'));
    }

}
