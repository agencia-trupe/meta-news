<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTextosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('textos', function(Blueprint $table)
		{
			$table->engine = 'MYISAM';
			$table->increments('id');
			$table->integer('textos_categorias_id');	
			$table->string('titulo');
			$table->string('slug');
			$table->string('capa');
			$table->string('imagem_destaque');
			$table->string('video_destaque');
			$table->date('data_publicacao');
			$table->text('olho');
			$table->text('texto');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('textos');
	}

}
