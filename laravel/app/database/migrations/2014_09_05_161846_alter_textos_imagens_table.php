<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTextosImagensTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('textos_imagens', function(Blueprint $table)
		{
			$table->string('textos_id')->after('id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('textos_imagens', function(Blueprint $table)
		{
			$table->dropColumn('textos_id');
		});
	}

}
