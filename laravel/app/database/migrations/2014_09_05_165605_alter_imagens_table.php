<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterImagensTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('textos_imagens', function(Blueprint $table)
		{
			$table->integer('ordem');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('textos_imagens', function(Blueprint $table)
		{
			$table->dropColumn('ordem');
		});
	}

}
