<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerguntasFrequentesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('perguntas_frequentes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->increments('categoria_id');
			$table->integer('ordem')->default(0);
			$table->text('pergunta');
			$table->text('resposta');
			$table->timestamps();
		});

		Schema::create('perguntas_frequentes_categorias', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('ordem')->default(0);
			$table->string('titulo');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('perguntas_frequentes');
	}

}
