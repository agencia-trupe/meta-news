<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTextosCategoriasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('textos_categorias', function(Blueprint $table)
		{
			$table->string('singular')->after('titulo');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('textos_categorias', function(Blueprint $table)
		{
			$table->dropColumn('singular');
		});
	}

}
