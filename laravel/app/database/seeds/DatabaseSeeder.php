<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		//$this->call('ContatoSeeder');
		//$this->call('QuemSomosSeeder');
		//$this->call('CategoriasSeeder');
		//$this->call('UsersSeeder');
		$this->call('PoliticaSeeder');
	}

}
