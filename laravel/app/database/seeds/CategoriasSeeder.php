<?php

class CategoriasSeeder extends Seeder {

    public function run()
    {
        $data = array(
            array(
                'titulo' => 'Notícias',
                'singular' => 'Notícia'
                'slug' => 'noticias',
            ),
            array(
                'titulo' => 'Eventos',
                'singular' => 'Evento'
                'slug' => 'eventos',
            ),
            array(
                'titulo' => 'Artigos',
                'singular' => 'Artigo'
                'slug' => 'artigos',
            ),
            array(
                'titulo' => 'Dicas',
                'singular' => 'Dica'
                'slug' => 'dicas',
            ),
            array(
                'titulo' => 'Pesquisas',
                'singular' => 'Pesquisa'
                'slug' => 'pesquisas',
            ),
            array(
                'titulo' => 'Reportagens',
                'singular' => 'Reportagem'
                'slug' => 'reportagens',
            ),
            array(
                'titulo' => 'Entrevistas',
                'singular' => 'Entrevista'
                'slug' => 'entrevistas',
            ),
        );

        DB::table('textos_categorias')->insert($data);
    }

}
