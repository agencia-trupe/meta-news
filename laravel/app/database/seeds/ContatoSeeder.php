<?php

class ContatoSeeder extends Seeder {

    public function run()
    {
        $data = array(
            array(
				'texto' => '<p>Texto de entrada</p>',
                'prefixo_telefone' => '+55 11',
                'telefone' => '5555-4444',
                'email' => 'contato@metanews.com.br'
            )
        );

        DB::table('contato')->insert($data);
    }

}
