<?php
class EnquetesHelper {

	public static function porcentagemDeVotos($id_opcao)
	{
		$opcao = EnqueteOpcoes::find($id_opcao);
		$questao = EnquetePergunta::find($opcao->enquete_pergunta_id);

		$votos = sizeof($opcao->votos);
    	$total = sizeof($questao->numeroVotos);
    	$retorno = ($total > 0) ? 100 * round(($votos/$total), 2) : 0;
    	return $retorno.'%';
	}

	public static function totalDeVotos($id_enquete){
		$questao = EnquetePergunta::find($id_enquete);

		$resultado = sizeof($questao->numeroVotos);

		if($resultado == 1)
			return "1 voto";
		else
			return $resultado." votos";
	}

	public static function votosNaOpcao($id_opcao){
		$opcao = EnqueteOpcoes::find($id_opcao);
		
		$resultado = sizeof($opcao->votos);
		
		if($resultado == 1)
			return "1 voto";
		else
			return $resultado." votos";
	}

}